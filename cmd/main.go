package main

import (
	"context"
	"flag"

	"gitlab.com/kamee/picverse/database"
	"gitlab.com/kamee/picverse/logger"
	"gitlab.com/kamee/picverse/routes"
	"gitlab.com/kamee/picverse/services"
	"gitlab.com/kamee/picverse/views"
)

func run(ctx context.Context, srv *services.Server, query []services.Queries) {
	picverse := flag.String("picverse", "none", "manage tables")
	actor := flag.String("actor", "none", "create new actor")
	note := flag.String("note", "none", "create new note")
	draft := flag.String("draft", "none", "handle drafts")
	show := flag.String("show", "none", "show content")
	flag.Parse()

	switch *picverse {
	case "run":
		_ = views.Template(ctx, srv)
		routes.Server(srv)
	case "table":
		srv.CreateTables(ctx, query)
		srv.CreateIndex(ctx)
	}

	switch *actor {
	case "create":
		srv.NewActor(ctx)
	case "update":
		srv.Update(ctx, "actor")
	case "description":
		srv.Update(ctx, "description")
	case "delete":
		srv.Delete(ctx, "actor")
	}

	switch *note {
	case "create":
		srv.NewNote(ctx)
	case "load":
		srv.Load(ctx, "note")
	case "img":
		srv.CreatePhotoList(ctx)
	case "delete":
		srv.Delete(ctx, "note")
	}

	switch *draft {
	case "show":
		srv.NewNote(ctx)
	case "load":
		srv.Load(ctx, "draft")
	case "delete":
		srv.Delete(ctx, "draft")
	case "update":
		srv.Update(ctx, "draft")
	}

	switch *show {
	case "outbox":
		srv.Fetch(ctx, "outbox")
	case "drafts":
		srv.Fetch(ctx, "drafts")
	case "actors":
		srv.Fetch(ctx, "actors")
	}
}

func main() {
	logger.StartLogger()
	ctx := context.Background()
	db := database.Connection(ctx)
	actor := &services.Actor{db}
	outboxes := &services.Outboxes{db}
	drafts := &services.Drafts{db}
	followers := &services.Followers{db}
	inboxes := &services.Inboxes{db}
	// i am brain dead pls suggest me a solution for this ugly code
	// *i dont want to use reflect *
	srv := services.NewServer(actor, outboxes, drafts, followers, inboxes)
	query := []services.Queries{actor, outboxes, drafts, followers, inboxes}
	run(ctx, srv, query)
}
