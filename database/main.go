package database

import (
	"context"
	"github.com/jackc/pgx/v5/pgxpool"
	"sync"

	"gitlab.com/kamee/picverse/config"
	"gitlab.com/kamee/picverse/logger"
)

type Repository struct {
	Conn *pgxpool.Pool
}

var (
	pgInstance *Repository
	pgOnce     sync.Once
)

func Connection(ctx context.Context) *Repository {
	pgOnce.Do(func() {
		db, err := pgxpool.New(ctx, config.DBConfig().DSN)
		if err != nil {
			logger.Critical.Fatal(err)
		}

		pgInstance = &Repository{db}
	})

	return pgInstance
}
