package database

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v5"
	"gitlab.com/kamee/picverse/logger"
)

func (db *Repository) CreateActorsTable(ctx context.Context, tx pgx.Tx) error {
	_query := fmt.Sprintf(`create table if not exists actors(
	    id           bigserial,
            activity_id  text not null primary key,
            object       jsonb      not null,
	    role         text
    );`)

	logger.Info.Println("Creating the actor's table: ", _query)
	if _, err := tx.Exec(ctx, _query); err != nil {
		return err
	}

	return nil
}

func (db *Repository) CreateOutboxesTable(ctx context.Context, tx pgx.Tx) error {
	_query := fmt.Sprintf(`create table if not exists outboxes(
	    id     bigserial primary key,
	    activity_id text not null,
            actor  text not null,
	    title  text not null,
            object jsonb      not null,
	    tags   text[],
	    created_at timestamp with time zone NOT NULL DEFAULT now()
    );`)

	logger.Info.Println("Creating the outbox table: ", _query)
	if _, err := tx.Exec(ctx, _query); err != nil {
		return err
	}

	return nil
}

func (db *Repository) CreateDraftsTable(ctx context.Context, tx pgx.Tx) error {
	_query := fmt.Sprintf(`create table if not exists drafts(
        id     bigserial,
	activity_id text not null,
        actor  text       not null,
	title  text       not null,
        object jsonb      not null,
	tags   text[],
	created_at timestamp with time zone NOT NULL DEFAULT now()
    );`)

	logger.Info.Println("Creating the draft table: ", _query)
	if _, err := tx.Exec(ctx, _query); err != nil {
		return err
	}

	return nil
}

func (db *Repository) CreateFollowersTable(ctx context.Context, tx pgx.Tx) error {
	_query := fmt.Sprintf(`create table if not exists remote_actors(
        id           bigserial primary key,
	remote_actor text not null unique
    );`)

	logger.Info.Println("Creating the follower table: ", _query)
	if _, err := tx.Exec(ctx, _query); err != nil {
		return err
	}

	return nil
}

func (db *Repository) CreateActorFollowersTable(ctx context.Context, tx pgx.Tx) error {
	_query := fmt.Sprintf(`CREATE TABLE IF NOT EXISTS actor_followers(
	actor_id TEXT REFERENCES actors(activity_id),
	follower_id INT REFERENCES remote_actors(id),
	PRIMARY KEY (actor_id, follower_id)
    );`)

	logger.Info.Println("Creating the actor's followers table: ", _query)
	if _, err := tx.Exec(ctx, _query); err != nil {
		return err
	}

	return nil
}

func (db *Repository) CreateRepliesTable(ctx context.Context, tx pgx.Tx) error {
	_query := fmt.Sprintf(`create table if not exists replies(
	  reply_id text not null,
          note_id  text not null,
          reply jsonb  not null
       );`)

	logger.Info.Println("Creating the replies table: ", _query)
	if _, err := tx.Exec(ctx, _query); err != nil {
		return err
	}

	return nil
}

func (db *Repository) CreateNoteIDindex(ctx context.Context, tx pgx.Tx) error {
	_query := fmt.Sprintf(`create index on replies(note_id);`)
	logger.Info.Println("Creating the note id index: ", _query)
	if _, err := tx.Exec(ctx, _query); err != nil {
		return err
	}

	return nil
}

func (db *Repository) CreateActorIndex(ctx context.Context, tx pgx.Tx) error {
	_query := fmt.Sprintf(`create index on outboxes(actor);`)
	logger.Info.Println("Creating the actor's index: ", _query)
	if _, err := tx.Exec(ctx, _query); err != nil {
		return err
	}

	return nil
}

func (db *Repository) InsertActor(ctx context.Context, tx pgx.Tx, _actor string, _object []byte, role string) error {
	_query := fmt.Sprintf(`
        insert into actors(activity_id, object, role) values ($1, $2, $3) on conflict (activity_id) do nothing;`)

	if _, err := tx.Exec(ctx, _query, _actor, _object, role); err != nil {
		return err
	}

	return nil
}

// TODO: change tag part
func (db *Repository) InsertOutbox(ctx context.Context, tx pgx.Tx, _actor, _id, title string, _object []byte, tags []string) (int, error) {
	var id int
	if len(tags) != 0 {
		_query := fmt.Sprintf(`
         insert into outboxes(actor, activity_id, title, object, tags) values ($1, $2, $3, $4, $5) returning id;`)

		if err := tx.QueryRow(ctx, _query, _actor, _id, title, _object, tags).Scan(&id); err != nil {
			return -1, err
		}
	} else {
		_query := fmt.Sprintf(`
         insert into outboxes(actor, activity_id, title, object) values ($1, $2, $3, $4) returning id;`)
		if err := tx.QueryRow(ctx, _query, _actor, _id, title, _object).Scan(&id); err != nil {
			return -1, err
		}
	}

	return id, nil
}

func (db *Repository) InsertReply(ctx context.Context, tx pgx.Tx, reply_id, note_id string, object []byte) error {
	_query := fmt.Sprintf(`
        insert into replies(reply_id, note_id, reply) values ($1, $2, $3);
    `)

	if _, err := tx.Exec(ctx, _query, reply_id, note_id, object); err != nil {
		return err
	}

	return nil
}

func (db *Repository) InsertFollower(ctx context.Context, tx pgx.Tx, remote_actor string) error {
	_query := fmt.Sprintf(`
        insert into remote_actors(remote_actor) values($1) on conflict(remote_actor) do nothing;
    `)

	if _, err := tx.Exec(ctx, _query, remote_actor); err != nil {
		return err
	}

	return nil
}

func (db *Repository) InsertActorFollowers(ctx context.Context, tx pgx.Tx, actor_id string, follower_id int) error {
	_query := fmt.Sprintf(`insert into actor_followers(actor_id, follower_id) values ($1, $2);`)

	if _, err := tx.Exec(ctx, _query, actor_id, follower_id); err != nil {
		return err
	}

	return nil
}

func (db *Repository) InsertDrafts(ctx context.Context, tx pgx.Tx, _actor, _id, title string, _object []byte, tags []string) error {
	_query := fmt.Sprintf(`
        insert into drafts(actor, activity_id, title, object, tags) values ($1, $2, $3, $4, $5);`)

	if _, err := tx.Exec(ctx, _query, _actor, _id, title, _object, tags); err != nil {
		return err
	}

	return nil
}

func (db *Repository) UpdateDraft(ctx context.Context, tx pgx.Tx, activity_id string) error {
	_query := fmt.Sprintf(`WITH moved_rows AS (
	DELETE FROM drafts
	  WHERE
	    activity_id = $1
        RETURNING *
        )
        INSERT INTO outboxes
	SELECT * FROM moved_rows;`)

	if _, err := tx.Exec(ctx, _query, activity_id); err != nil {
		return err
	}

	return nil
}

func (db *Repository) UpdateActor(ctx context.Context, tx pgx.Tx, _clm, old_value, new_value string) (err error) {
	switch _clm {
	case "username":
		err = db.update_username(ctx, tx, old_value, new_value)
	case "summary":
		err = db.update_summary(ctx, tx, old_value, new_value)
	}

	return err
}

func (db *Repository) update_username(ctx context.Context, tx pgx.Tx, username, new_username string) error {
	_query := fmt.Sprintf(`
        update actors set object = object :: jsonb || '{"name": "%s"}' where activity_id = $1;`,
		new_username)

	if _, err := tx.Exec(ctx, _query, username); err != nil {
		return err
	}

	return nil
}

func (db *Repository) update_summary(ctx context.Context, tx pgx.Tx, actor_id, description string) error {
	_query := fmt.Sprintf(`
        UPDATE actors set object = object :: jsonb || '{"summary": "%s"}' where activity_id = $1;`,
		description)

	if _, err := tx.Exec(ctx, _query, actor_id); err != nil {
		return err
	}

	return nil
}

// TODO: naming
func (db *Repository) SelectActors(ctx context.Context, tx pgx.Tx) (obj []map[string]interface{}, err error) {
	var activity_id string
	var actor_url interface{}

	_query := fmt.Sprintf(`select activity_id, object-> 'id' from actors;`)
	rows, err := tx.Query(ctx, _query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		rows.Scan(&activity_id, &actor_url)
		_rows := map[string]interface{}{
			"actor": activity_id,
			"url":   actor_url,
		}
		obj = append(obj, _rows)
	}

	return obj, err
}

func (db *Repository) FetchActors(ctx context.Context, tx pgx.Tx) (objects []map[interface{}]interface{}, err error) {
	var id, actor interface{}
	_query := fmt.Sprintf(`SELECT id, actor from actors;`)

	rows, err := tx.Query(ctx, _query)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		rows.Scan(&id, &actor)
		_rows := map[interface{}]interface{}{
			id: actor,
		}
		objects = append(objects, _rows)
	}

	return objects, err
}

func (db *Repository) Fetch(ctx context.Context, tx pgx.Tx, _tbl string) (objects []map[interface{}]interface{}, err error) {
	var actor, title interface{}
	_query := fmt.Sprintf(`SELECT actor, title FROM %s ORDER BY created_at DESC;`, _tbl)

	rows, err := tx.Query(ctx, _query)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		rows.Scan(&actor, &title)
		_rows := map[interface{}]interface{}{
			actor: title,
		}
		objects = append(objects, _rows)
	}

	return objects, err
}

func (db *Repository) FetchByTag(ctx context.Context, tx pgx.Tx, tag string, offset, limit int) (objects []map[string]interface{}, count int, err error) {
	_query := fmt.Sprintf(`select object from outboxes where $1 = ANY(tags) order by created_at desc offset %d limit %d;`, offset, limit)

	rows, err := tx.Query(ctx, _query, tag)
	if err != nil {
		return nil, -1, err
	}

	defer rows.Close()

	for rows.Next() {
		var object map[string]interface{}
		rows.Scan(&object)
		objects = append(objects, object)
		count++
	}

	return
}

func (db *Repository) FetchTags(ctx context.Context, tx pgx.Tx) (tags []string, err error) {
	var tag []string
	_query := fmt.Sprintf(`select tags from outboxes;`)

	rows, err := tx.Query(ctx, _query)
	if err != nil {
		return nil, err
	}

	defer rows.Close()
	for rows.Next() {
		rows.Scan(&tag)
		tags = append(tags, tag...)
	}

	return tags, err
}

func (db *Repository) FetchFollowers(ctx context.Context, tx pgx.Tx, _actor string) (objects []string, err error) {
	var follower string
	_query := fmt.Sprintf(`
        select remote_actor 
	from actor_followers AF
	join actors A on AF.actor_id = A.activity_id
	join remote_actors RA ON RA.id = AF.follower_id
	where AF.actor_id = $1;
    `)

	rows, err := tx.Query(ctx, _query, _actor)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		rows.Scan(&follower)
		objects = append(objects, follower)
	}

	return objects, err
}

func (db *Repository) FetchDraftData(ctx context.Context, tx pgx.Tx, activity_id string) (actor string, object map[string]interface{}, err error) {
	_query := fmt.Sprintf(`
        select actor, object from drafts where activity_id = $1;
    `)

	if err := tx.QueryRow(ctx, _query, activity_id).Scan(&actor, &object); err != nil {
		return "", nil, err
	}

	return actor, object, err
}

func (db *Repository) FetchAll(ctx context.Context, tx pgx.Tx, offset, limit int) (objects []map[string]interface{}, err error) {
	_query := fmt.Sprintf(`select object from outboxes order by created_at desc offset %d limit %d;`, offset, limit)

	rows, err := tx.Query(ctx, _query)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var object map[string]interface{}
		rows.Scan(&object)
		objects = append(objects, object)
	}

	return objects, err
}

func (db *Repository) FetchAllForActor(ctx context.Context, tx pgx.Tx, _actor string) (objects []map[string]interface{}, err error) {
	_query := fmt.Sprintf(`SELECT object FROM outboxes WHERE actor = $1;`)

	rows, err := tx.Query(ctx, _query, _actor)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var object map[string]interface{}
		rows.Scan(&object)
		objects = append(objects, object)
	}

	return objects, err
}

func (db *Repository) FetchForActor(ctx context.Context, tx pgx.Tx, _actor string, offset, limit int) (objects []map[string]interface{}, err error) {
	_query := fmt.Sprintf(`SELECT object FROM outboxes WHERE actor = $1 offset %d limit %d;`, offset, limit)

	rows, err := tx.Query(ctx, _query, _actor)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var object map[string]interface{}
		rows.Scan(&object)
		objects = append(objects, object)
	}

	return objects, err
}

func (db *Repository) FetchByTitle(ctx context.Context, tx pgx.Tx, _tbl, _title string) (content []map[string]string, err error) {
	_query := fmt.Sprintf(`
        select activity_id, object-> 'object' -> 'content' from %s where title = $1;`, _tbl)

	rows, err := tx.Query(ctx, _query, _title)

	if err != nil {
		return nil, err
	}

	defer rows.Close()
	for rows.Next() {
		var id, text string
		rows.Scan(&id, &text)
		content_ := map[string]string{
			id: text,
		}

		content = append(content, content_)
	}

	return content, err
}

func (db *Repository) FetchNoteByID(ctx context.Context, tx pgx.Tx, _id string) (object map[string]interface{},
	actor, title string,
	tags []string,
	err error) {
	_box_query := fmt.Sprintf(`
        select actor, title, tags, object from outboxes where activity_id = $1;`)

	if err := tx.QueryRow(ctx, _box_query, _id).Scan(&actor, &title, &tags, &object); err != nil {
		return nil, "", "", nil, err
	}

	return object, actor, title, tags, err
}

func (db *Repository) FetchByID(ctx context.Context, tx pgx.Tx, _tbl, _id string) (object map[string]interface{}, role string, err error) {
	_box_query := fmt.Sprintf(`
        select object, role from %s where activity_id = $1;
   `, _tbl)

	if err := tx.QueryRow(ctx, _box_query, _id).Scan(&object, &role); err != nil && err != pgx.ErrNoRows {
		return nil, "", err
	}

	return object, role, err
}

func (db *Repository) FetchID(ctx context.Context, tx pgx.Tx, _val string) (id int, err error) {
	_query := fmt.Sprintf(`select id from remote_actors where remote_actor = $1;`)

	if err = tx.QueryRow(ctx, _query, _val).Scan(&id); err != nil {
		return -1, err
	}

	return id, err
}

func (db *Repository) FetchReplies(ctx context.Context, tx pgx.Tx, id string) (collection []map[string]interface{}, count int, err error) {
	_query := fmt.Sprintf(`
        select reply from replies where note_id = $1;
    `)

	rows, err := tx.Query(ctx, _query, id)
	if err != nil && err != pgx.ErrNoRows {
		return nil, -1, err
	}
	defer rows.Close()

	for rows.Next() {
		var object map[string]interface{}
		rows.Scan(&object)
		collection = append(collection, object)
		count++
	}

	return
}

func (db *Repository) Exists(ctx context.Context, tx pgx.Tx, _tbl, _val string) (bool, error) {
	var exists bool
	var _query string
	if _tbl == "followers" {
		_query = fmt.Sprintf(`select exists(select from followers where follower = $1);`)
	} else if _tbl == "replies" {
		_query = fmt.Sprintf(`select exists(select from replies where reply_id = $1);
	`)
	} else {
		_query = fmt.Sprintf(`select exists(select from %s where activity_id = $1);
        `, _tbl)
	}

	if err := tx.QueryRow(ctx, _query, _val).Scan(&exists); err != nil {
		return false, err
	}

	return exists, nil
}

func (db *Repository) DeleteActorFollower(ctx context.Context, tx pgx.Tx, actor string, remote_actor int) error {
	_query := `DELETE FROM actor_followers WHERE actor_id = $1 AND follower_id = $2;`

	if _, err := tx.Exec(ctx, _query, actor, remote_actor); err != nil {
		return err
	}

	return nil
}

// TODO: reference actor(id) -> outboxes, followers, inboxes, etc.
func (db *Repository) Delete(ctx context.Context, tx pgx.Tx, _tbl string, _val string) error {
	switch _tbl {
	case "actor":
		delete_followers := `DELETE FROM actor_followers WHERE actor_id = $1;`
		delete_actors := `DELETE FROM actors WHERE activity_id = $1`
		delete_outbox := `DELETE FROM outboxes WHERE actor = $1`

		if _, err := tx.Exec(ctx, delete_followers, _val); err != nil {
			return err
		}
		if _, err := tx.Exec(ctx, delete_actors, _val); err != nil {
			return err
		}
		if _, err := tx.Exec(ctx, delete_outbox, _val); err != nil {
			return err
		}
	case "note":
		_query := `DELETE FROM outboxes WHERE activity_id = $1;`
		if _, err := tx.Exec(ctx, _query, _val); err != nil {
			return err
		}
	case "draft":
		_query := `DELETE FROM drafts WHERE activity_id = $1;`
		if _, err := tx.Exec(ctx, _query, _val); err != nil {
			return err
		}
	}

	return nil
}

func (db *Repository) GetCountAll(ctx context.Context, tx pgx.Tx, _tbl string) (count int, err error) {
	_query := fmt.Sprintf(`select count(*) from %s`, _tbl)

	if err = tx.QueryRow(ctx, _query).Scan(&count); err != nil {
		return -1, err
	}

	return count, err
}

func (db *Repository) GetCountActor(ctx context.Context, tx pgx.Tx, _actor string) (count int, err error) {
	_query := fmt.Sprintf(`select count(*) from outboxes where actor = $1`)

	if err = tx.QueryRow(ctx, _query, _actor).Scan(&count); err != nil {
		return -1, err
	}

	return count, err
}
