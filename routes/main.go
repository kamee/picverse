package routes

import (
	"github.com/labstack/echo/v4"
	_ "github.com/labstack/echo/v4/middleware"
	"gitlab.com/kamee/picverse/services"
	"html/template"
	"io"
)

type Handlers struct {
	Srv *services.Server
}

type Template struct {
	templates *template.Template
}

func (t *Template) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	return t.templates.ExecuteTemplate(w, name, data)
}

func Server(srv *services.Server) {
	handle := &Handlers{srv}
	port := ":8080"

	t := &Template{
		templates: template.Must(template.ParseGlob("views/templates/*.tmpl")),
	}

	e := echo.New()
	e.Renderer = t
	e.Static("/static", "static")
	e.File("/favicon.ico", "static/favicon.ico")
	e.GET("/.well-known/webfinger", handle.webfinger_handler)
	e.GET("/.well-known/host-meta", handle.hostmeta_handler)
	e.GET("/.well-known/nodeinfo", handle.nodeinfo_handler)
	e.GET("/nodeinfo/2.1", handle.nodeconfs_handler)
	e.GET("/", handle.home_handler)
	e.GET("/:actor", handle.actor_handler)
	e.GET("/:actor/outbox", handle.outbox_handler)
	e.POST("/:actor/inbox", handle.inbox_handler)
	e.GET("/:actor/followers", handle.followers_handler)
	e.GET("/:actor/note/:id", handle.note_handler)
	e.GET("/:actor/note/:id/replies", handle.replies_handler)
	e.GET("/tags", handle.tags_handler)
	e.GET("/tags/:tag", handle.tag_handler)
	e.GET("/images/:path", handle.images_handler)
	e.GET("/image/:path", handle.image_handler)
	e.GET("/atom", handle.atom_handler)
	e.Logger.Fatal(e.Start(port))
}
