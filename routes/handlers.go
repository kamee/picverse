package routes

import (
	"context"
	"encoding/json"
	"github.com/labstack/echo/v4"
	"net/http"
	"os"
	"strings"

	"gitlab.com/kamee/picverse/activitypub"
	"gitlab.com/kamee/picverse/config"
	"gitlab.com/kamee/picverse/logger"
	"gitlab.com/kamee/picverse/views"
)

// type page_key string
var page_key = "page"

func (handle *Handlers) webfinger_handler(c echo.Context) error {
	var data map[string]interface{}
	resource := c.QueryParam("resource")
	if resource == "" {
		return echo.NewHTTPError(http.StatusBadRequest, "The 'resource' query string parameter is required.")
	}

	webfinger := activitypub.Webfinger(resource)
	_finger, err := json.Marshal(webfinger)
	if err != nil {
		logger.Error.Println(err)
		return echo.NewHTTPError(http.StatusBadRequest, "Bad request")
	}

	json.Unmarshal(_finger, &data)
	logger.Info.Println(data)

	c.Response().Header().Set("Cache-Control", "public, max-age=86400")
	c.Response().Header().Set("Content-Type", "application/ld+json")
	c.Response().Header().Set("Content-Type", "application/jrd+json")
	return c.JSON(http.StatusOK, data)
}

func (handle *Handlers) hostmeta_handler(c echo.Context) error {
	return c.XML(http.StatusOK, activitypub.Hostmeta())
}

func (handle *Handlers) nodeconfs_handler(c echo.Context) error {
	result := config.PodConfig()
	c.Response().Header().Set("Content-Type", "application/ld+json")
	return c.JSON(http.StatusOK, result)
}

func (handle *Handlers) nodeinfo_handler(c echo.Context) error {
	var data map[string]interface{}
	nodeinfo := activitypub.NodeInfo()
	_data, err := json.Marshal(nodeinfo)
	if err != nil {
		logger.Error.Println(err)
		return echo.NewHTTPError(http.StatusBadRequest, "Bad request")
	}

	json.Unmarshal(_data, &data)
	c.Response().Header().Set("Cache-Control", "public, max-age=86400")
	c.Response().Header().Set("Content-Type", "application/ld+json")
	c.Response().Header().Set("Content-Type", "application/json; profile='http://nodeinfo.diaspora.software/ns/schema/2.1#'")
	c.Response().WriteHeader(http.StatusOK)
	return c.JSON(http.StatusOK, data)
}

func (handle *Handlers) actor_handler(c echo.Context) error {
	actor := c.Param("actor")
	page := c.QueryParam("page")
	headers := c.Request().Header
	if page == "" {
		page = "1"
	}

	ctx := context.WithValue(c.Request().Context(), page_key, page)
	data, role, err := handle.Srv.User.FetchByID(ctx, actor)
	if err != nil || len(data) == 0 {
		return echo.NewHTTPError(http.StatusInternalServerError, "Internal Server Error")
	}

	accept_type := headers.Get("Accept")
	content_type := headers.Get("Content-type")

	switch {
	case strings.Contains(content_type, "json"), strings.Contains(accept_type, "json"):
		c.Response().Header().Set("Content-Type", "application/activity+json")
		return c.JSON(http.StatusOK, data)
	default:
		c.Response().Header().Set("Content-Type", "text/html")
		return c.Render(http.StatusOK, role, views.ConditionalRendering(ctx, actor, data, role))
	}
}

func (handle *Handlers) note_handler(c echo.Context) error {
	id := c.Param("id")
	headers := c.Request().Header
	data, actor, title, _, err := handle.Srv.Outbox.FetchByID(c.Request().Context(), id)
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, "Unknown error")
	}

	if len(data) == 0 {
		return echo.NewHTTPError(http.StatusNotFound, "Note not found")
	}

	replies, err := handle.Srv.Inbox.Fetch(c.Request().Context(), id)
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, "Unknown error")
	}

	content_type := headers.Get("Content-type")
	accept_type := headers.Get("Accept")

	content_type, ok := c.Get("Content-type").(string)
	if !ok {
		content_type = ""
	}

	switch {
	case strings.Contains(content_type, "json"), strings.Contains(accept_type, "json"):
		//	c.Response().WriteHeader(http.StatusOK)
		c.Response().Header().Set("Content-type", "application/activity+json")
		return c.JSON(http.StatusOK, data)
	default:
		c.Response().Header().Set("Content-Type", "text/html")
		return c.Render(http.StatusOK, "page", views.RenderPage(id, title, actor, data, replies))
	}
}

func (handle *Handlers) replies_handler(c echo.Context) error {
	id := c.Param("id")
	data, err := handle.Srv.Inbox.Fetch(c.Request().Context(), id)
	if err != nil {
		return echo.NewHTTPError(http.StatusNotFound, "Reply not found")
	}

	content_type, ok := c.Get("Content-type").(string)
	if !ok {
		content_type = ""
	}

	switch {
	case strings.Contains(content_type, "json"):
		c.Response().Header().Set("Content-Type", "application/activity+json")
		c.Response().WriteHeader(http.StatusOK)
		return c.JSON(http.StatusOK, data)
	default:
		c.Response().Header().Set("Content-Type", "text/html")
		return c.Render(http.StatusOK, "page", views.RenderReplies(data))

	}
}

func (handle *Handlers) inbox_handler(c echo.Context) error {
	var activity map[string]interface{}
	err := json.NewDecoder(c.Request().Body).Decode(&activity)
	if err != nil {
		logger.Error.Println(err)
		return echo.NewHTTPError(http.StatusBadRequest, "Bad Request")
	}

	logger.Info.Println(c.Request())

	if activitypub.VerifyReq(c.Request(), activity) == nil {
		switch activity["type"] {
		case "Follow":
			handle.Srv.Follower.Autoaccept(c.Request().Context(), activity)
		case "Undo":
			handle.Srv.Follower.Delete(c.Request().Context(), activity)
		case "Create":
			handle.Srv.Inbox.Process(c.Request().Context(), activity)
		}
	} else {
		return echo.NewHTTPError(http.StatusBadRequest, "Bad Request")
	}

	return c.JSON(http.StatusOK, nil)
}

func (handle *Handlers) outbox_handler(c echo.Context) error {
	actor := c.Param("actor")

	ctx := c.Request().Context()
	data, err := handle.Srv.Outbox.FetchAllForActor(ctx, actor)
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, "Internal Server Error")
	}

	if data.TotalItems == 0 {
		return echo.NewHTTPError(http.StatusNotFound, "Note not found")
	}

	c.Response().Header().Set("Content-Type", "application/activity+json")
	c.Response().WriteHeader(http.StatusOK)
	return c.JSON(http.StatusOK, data)
}

func (handle *Handlers) tags_handler(c echo.Context) error {
	data, err := handle.Srv.Outbox.FetchTags(c.Request().Context())
	if err != nil {
		logger.Error.Println(err)
		return echo.NewHTTPError(http.StatusInternalServerError, "Internal Server Error")
	}

	content_type, ok := c.Get("Content-type").(string)
	if !ok {
		content_type = ""
	}

	switch {
	case strings.Contains(content_type, "json"):
		c.Response().Header().Set("Content-Type", "application/activity+json")
		c.Response().WriteHeader(http.StatusOK)
		return c.JSON(http.StatusOK, data)
	default:
		c.Response().Header().Set("Content-Type", "text/html")
		return c.Render(http.StatusOK, "tags", views.RenderTags(data))
	}
}

func (handle *Handlers) tag_handler(c echo.Context) error {
	tag := c.Param("tag")
	page := c.QueryParam("page")
	if page == "" {
		page = "1"
	}

	ctx := context.WithValue(c.Request().Context(), page_key, page)
	data, err := handle.Srv.Outbox.FetchByTag(ctx, tag)
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, "Internal Server Error")
	}

	content_type, ok := c.Get("Content-type").(string)
	if !ok {
		content_type = ""
	}

	switch {
	case strings.Contains(content_type, "json"):
		c.Response().Header().Set("Content-Type", "application/activity+json")
		c.Response().WriteHeader(http.StatusOK)
		return c.JSON(http.StatusOK, data)
	default:
		c.Response().Header().Set("Content-Type", "text/html")
		return c.Render(http.StatusOK, "tag_page", views.RenderTagFeed(ctx, data))
	}
}

func (handle *Handlers) home_handler(c echo.Context) error {
	page := c.QueryParam("page")
	if page == "" {
		page = "1"
	}

	ctx := context.WithValue(c.Request().Context(), page_key, page)
	data, err := handle.Srv.Outbox.FetchAll(ctx)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, "Bad request")
	}

	content_type, ok := c.Get("Content-type").(string)
	if !ok {
		content_type = ""
	}

	switch {
	case strings.Contains(content_type, "json"):
		c.Response().Header().Set("Content-Type", "application/activity+json")
		return c.JSON(http.StatusOK, data)
	default:
		c.Response().Header().Set("Content-Type", "text/html")
		return c.Render(http.StatusOK, "home", views.RenderHome(ctx, data))
	}
}

func (handle *Handlers) followers_handler(c echo.Context) error {
	actor := c.Param("actor")
	data, err := handle.Srv.Follower.Fetch(c.Request().Context(), actor)
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, "Internal Server Error")
	}

	content_type, ok := c.Get("Content-type").(string)
	if !ok {
		content_type = ""
	}

	switch {
	case strings.Contains(content_type, "json"):
		c.Response().Header().Set("Content-Type", "application/activity+json")
		c.Response().WriteHeader(http.StatusOK)
		return c.JSON(http.StatusOK, data)
	default:
		c.Response().Header().Set("Content-Type", "text/html")
		return c.Render(http.StatusOK, "followers", views.RenderFollowers(data))
	}
}

func (handle *Handlers) images_handler(c echo.Context) error {
	path := c.Param("path")
	file, err := os.Open("images/" + path)
	if err != nil {
		logger.Error.Println(err)
		return echo.NewHTTPError(http.StatusNotFound, "Not Found")
	}

	return c.Stream(http.StatusOK, "image/png", file)
}

func (handle *Handlers) image_handler(c echo.Context) error {
	logger.Info.Println("image_handler")
	path := c.Param("path")
	imgFile, err := os.Open("images/" + path)
	if err != nil {
		logger.Error.Println(err)
	}

	content_type, ok := c.Get("Content-type").(string)
	if !ok {
		content_type = ""
	}

	switch {
	case strings.Contains(content_type, "json"):
		img_path := c.Request().Host + c.Request().URL.Path
		img_metadata := views.ReadMetadata(imgFile)
		file := views.ImageFile{
			Path:     img_path,
			Metadata: img_metadata,
		}

		c.Response().Header().Set("Content-Type", "application/activity+json")
		return c.JSON(http.StatusOK, file)
	default:
		return c.Render(http.StatusOK, "image", views.RenderImage("images/"+path, imgFile))
	}
}

func (handle *Handlers) atom_handler(c echo.Context) error {
	feed, err := handle.Srv.GenerateAtomFeed(context.Background())
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, "Bad request")
	}

	c.Response().Header().Set("Content-Type", "application/atom+xml")
	c.Response().WriteHeader(http.StatusOK)
	return c.String(http.StatusOK, feed)
}

