package activitypub

import (
	"crypto"
	"crypto/x509"
	"encoding/json"
	"encoding/pem"
	"errors"
	"github.com/go-fed/httpsig"
	"net/http"
	"strings"

	"gitlab.com/kamee/picverse/logger"
)

func VerifyReq(r *http.Request, req_activity map[string]interface{}) error {
	var _sgtr []string
	var activity map[string]interface{}

	actor, ok := req_activity["actor"].(string)
	logger.Info.Println(actor)
	if !ok {
		logger.Error.Println(ok)
		return errors.New("Invalid actor")
	}

	verifier, err := httpsig.NewVerifier(r)
	if err != nil {
		logger.Error.Println(err)
		return err
	}

	resp, err := http.Get(actor + ".json")
	if err != nil {
		logger.Error.Println(err)
		return err
	}
	defer resp.Body.Close()

	err = json.NewDecoder(resp.Body).Decode(&activity)
	if err != nil {
		logger.Error.Println(err)
		return err
	}

	pk, ok := activity["publicKey"].(map[string]interface{})
	if !ok {
		logger.Error.Println(ok)
		return errors.New("Invalid public key")
	}

	pkPEM := pk["publicKeyPem"]
	signature := strings.Split(r.Header["Signature"][0], ",")

	m := make(map[string]interface{})
	for _, e := range signature {
		_sgtr = strings.Split(e, "=")
		m[_sgtr[0]] = _sgtr[1]
	}

	block, _ := pem.Decode([]byte(pkPEM.(string)))
	if block == nil {
		err := errors.New("failed to parse PEM block containing the public key")
		logger.Error.Println(err)
		return err
	}

	pub, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		err = errors.New("failed to parse DER encoded public key: " + err.Error())
		logger.Error.Println(err)
		return err
	}

	var algo httpsig.Algorithm = httpsig.RSA_SHA256
	var pubKey crypto.PublicKey = pub

	// The verifier will verify the Digest in addition to the HTTP signature
	return verifier.Verify(pubKey, algo)
}
