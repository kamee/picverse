package activitypub

import "gitlab.com/kamee/picverse/activitypub/vocabulary"

func CreateAccept(actor, whom string, activity map[string]interface{}) *vocabulary.Activity {
	accept := &vocabulary.Activity{
		Context: vocabulary.ActivityStream,
		ID:      vocabulary.ID(),
		Type:    vocabulary.AcceptType,
		Actor:   actor,
		Object:  activity,
		To:      whom,
	}

	return accept
}
