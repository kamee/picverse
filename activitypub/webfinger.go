package activitypub

import (
	"fmt"
	"gitlab.com/kamee/picverse/activitypub/vocabulary"
)

func Webfinger(resource string) *vocabulary.Webfinger {
	link := vocabulary.AccountLink(resource)

	links := &[]vocabulary.Link{
		vocabulary.Link{
			Href: link,
			Rel:  vocabulary.WebfingerProfile,
			Type: "text/html",
		},
		vocabulary.Link{
			Href: link,
			Rel:  "self",
			Type: "application/activity+json",
		},
	}

	webfinger := &vocabulary.Webfinger{
		resource,
		[]string{link},
		links,
	}

	return webfinger
}

func Hostmeta() string {
	return fmt.Sprintf(`<?xml version="1.0" encoding="UTF-8"?>
        <XRD xmlns="http://docs.oasis-open.org/ns/xri/xrd-1.0">
        <Link rel="lrdd" type="application/xrd+xml" template="%s/.well-known/webfinger?resource={uri}"/>
        </XRD>`, vocabulary.Host)
}

func NodeInfo() map[string]interface{} {
	_ = vocabulary.NodeLink()
	nodeinfo := &[]vocabulary.NodeInfo{
		vocabulary.NodeInfo{
			Href: vocabulary.NodeLink(),
			Rel:  vocabulary.NodeSchema,
		},
	}

	links := map[string]interface{}{
		"links": nodeinfo,
	}

	return links
}
