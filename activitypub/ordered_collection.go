package activitypub

import "gitlab.com/kamee/picverse/activitypub/vocabulary"

func CreateOrderedCollection(items_count int, items []map[string]interface{}) *vocabulary.OrderedCollection {
	return &vocabulary.OrderedCollection{
		Context:      vocabulary.ActivityStream,
		Type:         vocabulary.CollectionType,
		TotalItems:   items_count,
		OrderedItems: items,
	}
}
