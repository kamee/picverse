package activitypub

import (
	"gitlab.com/kamee/picverse/activitypub/vocabulary"
)

func APActor(userid, username string, summary string) *vocabulary.APActor {
	pubkey := &vocabulary.KeyPair{
		ID:           vocabulary.PubkeyId(userid),
		Owner:        vocabulary.ActorId(userid),
		PublicKeyPem: vocabulary.PubkeyPem(),
	}

	actor := &vocabulary.APActor{
		Username:  userid,
		Name:      username,
		Summary:   summary,
		Inbox:     vocabulary.Box(userid, "inbox"),
		Outbox:    vocabulary.Box(userid, "outbox"),
		Followers: vocabulary.Box(userid, "followers"),
		Context: []string{
			vocabulary.ActivityStream,
			vocabulary.ActivityStreamSecurity,
		},
		ID:        vocabulary.ActorId(userid),
		Type:      vocabulary.ActorPerson,
		PublicKey: pubkey,
	}

	return actor
}
