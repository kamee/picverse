package vocabulary

type OrderedCollection struct {
	Context      string                   `json:"@context"`
	Type         string                   `json:"type"`
	TotalItems   int                      `json:"totalItems"`
	OrderedItems []map[string]interface{} `json:"orderedItems"`
}
