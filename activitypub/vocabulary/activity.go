package vocabulary

type Activity struct {
	Context string                 `json:"@context"`
	ID      string                 `json:"id"`
	Type    string                 `json:"type"`
	Actor   string                 `json:"actor"`
	Object  map[string]interface{} `json:"object"`
	To      string                 `json:"to"`
}
