package vocabulary

type Webfinger struct {
	Subject string   `json:"subject"`
	Aliases []string `json:"aliases"`
	Links   *[]Link  `json:"links"`
}

type Link struct {
	Href string `json:"href"`
	Rel  string `json:"rel"`
	Type string `json:"type"`
}
