package vocabulary

type APActor struct {
	Username  string   `json:"preferredUsername"`
	Name      string   `json:"name"`
	Summary   string   `json:"summary"`
	Inbox     string   `json:"inbox"`
	Outbox    string   `json:"outbox"`
	Followers string   `json:"followers"`
	Context   []string `json:"@context"`
	ID        string   `json:"id"`
	Type      string   `json:"type"`
	PublicKey *KeyPair `json:"publicKey"`
}
