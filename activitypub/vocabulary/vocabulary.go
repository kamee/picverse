package vocabulary

import (
	"crypto"
	"crypto/rsa"
	"fmt"
	"github.com/nu7hatch/gouuid"
	"golang.org/x/crypto/ssh"
	"io/ioutil"
	"strings"

	"gitlab.com/kamee/picverse/config"
	"gitlab.com/kamee/picverse/logger"
)

var (
	ActivityStream         = "https://www.w3.org/ns/activitystreams"
	ActivityStreamSecurity = "https://w3id.org/security/v1"
	PublicActivity         = "https://www.w3.org/ns/activitystreams#Public"
	NodeSchema             = "http://nodeinfo.diaspora.software/ns/schema/2.1"
	WebfingerProfile       = "http://webfinger.net/rel/profile-page"
	ActorPerson            = "Person"
	ActivityNote           = "Note"
	CreateType             = "Create"
	AcceptType             = "Accept"
	RejectType             = "Reject"
	HashtagType            = "Hashtag"
	ImageType              = "Image"
	CollectionType         = "OrderedCollection"
	Host                   = config.PodConfig().Host
)

func PubkeyId(username string) string {
	_id := fmt.Sprintf("%s/%s#main-key", Host, username)
	return _id
}

func TagID(tag string) (_id string) {
	_id = fmt.Sprintf("%s/tags/%s", Host, tag)
	return
}

func ActorId(username string) (_id string) {
	_id = fmt.Sprintf("%s/%s", Host, username)
	return
}

func NewID(username string) string {
	_id := fmt.Sprintf("%s/%s/%s", Host, username, generate_id())
	return _id
}

func NoteID(username string) string {
	_id := fmt.Sprintf("%s/%s/note/%s", Host, username, generate_id())
	return _id
}

func ID() string {
	_id := fmt.Sprintf("%s/%s", Host, generate_id())
	return _id
}

func generate_id() string {
	u4, err := uuid.NewV4()
	if err != nil {
		logger.Critical.Fatalf("error occured, this can cause federation issues: %s", err)
	}

	return u4.String()
}

func AccountLink(resource string) string {
	resource = resource[len("acct:")+1-1:]
	username := resource[:strings.IndexByte(resource, '@')]
	hostname := resource[strings.IndexByte(resource, '@')+1:]
	link := fmt.Sprintf("https://%s/%s", hostname, username)

	return link
}

func NodeLink() string {
	href := fmt.Sprintf("%s/nodeinfo/2.1", Host)
	return href
}

func PubkeyPem() string {
	publickey, err := ioutil.ReadFile("keys/public.pem")
	if err != nil {
		logger.Critical.Fatalf("error occured, this can cause federation issues: %s", err)
	}

	return string(publickey)
}

func PrivateKey() crypto.PrivateKey {
	var privateKey crypto.PrivateKey
	bytes, err := ioutil.ReadFile("keys/private.pem")
	key, err := ssh.ParseRawPrivateKey(bytes)
	privateKey = key.(*rsa.PrivateKey)
	if err != nil {
		logger.Critical.Fatalf("error occured, this can cause federation issues: %s", err)
	}

	return privateKey
}

func Box(username, _box string) (_id string) {
	_id = fmt.Sprintf("%s/%s/%s", Host, username, _box)
	return
}
