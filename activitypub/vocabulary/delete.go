package vocabulary

type Delete struct {
	Context string   `json:"@context"`
	Summary string   `json:"summary"`
	Type    string   `json:"type"`
	Actor   *APActor `json:"actor"`
	Object  string   `json:"object"`
	To      string   `json:"to"`
}
