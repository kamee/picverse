package vocabulary

type Collection struct {
	Context    string                   `json:"@context"`
	Type       string                   `json:"type"`
	TotalItems int                      `json:"totalItems"`
	Items      []map[string]interface{} `json:"items"`
}
