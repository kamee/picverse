package vocabulary

type Note struct {
	Context string `json:"@context"`
	ID      string `json:"id"`
	Type    string `json:"type"`
	Actor   string `json:"actor"`
	*Object `json:"object"`
}

type Tags struct {
	Href string `json:"href"`
	Name string `json:"name"`
	Type string `json:"type"`
}

type Attachment struct {
	Type      string `json:"type"`
	Content   string `json:"content"`
	Url       string `json:"url"`
	MediaType string `json:"mediaType"`
}

type Source struct {
	Content   string `json:"content"`
	MediaType string `json:"mediaType"`
}

type Object struct {
	ID           string        `json:"id"`
	Type         string        `json:"type"`
	AttributedTo string        `json:"attributedTo"`
	Content      string        `json:"content"`
	MediaType    string        `json:"mediaType"`
	Source       *Source       `json:"source"`
	Tag          []*Tags       `json:"tag"`
	Attachment   []*Attachment `json:"attachment,omitempty"`
	Published    string        `json:"published"`
	Summary      string        `json:"summary"`
	To           string        `json:"to"`
}
