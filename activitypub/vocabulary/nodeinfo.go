package vocabulary

type NodeInfo struct {
	Href string `json:"href"`
	Rel  string `json:"rel"`
}
