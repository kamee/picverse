package views

import (
	"context"
	"os"

	"gitlab.com/kamee/picverse/activitypub/vocabulary"
	"gitlab.com/kamee/picverse/helpers"
)

func RenderPage(id string, title, actor string, object map[string]interface{}, replies *vocabulary.Collection) *Blogpost {
	blogpost.Title = title
	blogpost.CurrentID = id
	blogpost.Object = object
	blogpost.Replies = replies
	bp := ParseContent(title, actor, object)

	return bp
}

func RenderHome(ctx context.Context, collection *vocabulary.OrderedCollection) *Blogpost {
	blogpost.Items = collection
	cp := helpers.GetCurrentPage(ctx)
	blogpost.CurrentPage = cp

	return blogpost
}

func ConditionalRendering(ctx context.Context, actor string, data map[string]interface{}, role string) *Blogpost {
	if role == "gallery" {
		return RenderGallery(ctx, actor, data)
	} else {
		return RenderActor(ctx, actor, data)
	}
}

func RenderActor(ctx context.Context, actor string, data map[string]interface{}) *Blogpost {
	blogpost.Actor = data
	blogpost.ActorPosts(ctx, actor)
	blogpost.Webfinger = helpers.BuildWebfinger(data["id"].(string))
	cp := helpers.GetCurrentPage(ctx)
	blogpost.CurrentPage = cp

	return blogpost
}

// TODO: pagination part is stupid and primitive, but i am bored
func RenderGallery(ctx context.Context, actor string, data map[string]interface{}) *Blogpost {
	blogpost.Actor = data
	blogpost.ActorAllPosts(ctx, actor)
	blogpost.Webfinger = helpers.BuildWebfinger(data["id"].(string))
	cp := helpers.GetCurrentPage(ctx)
	blogpost.CurrentPage = cp

	items := blogpost.Items.OrderedItems
	var imgs []string
	for el, _ := range items {
		attachment, _ := items[el]["object"].(map[string]interface{})["attachment"].([]interface{})
		for _, i := range attachment {
			_url := i.(map[string]interface{})["url"].(string)
			imgs = append(imgs, helpers.GetImagePath(_url))
		}
	}

	img_map := make(map[int][]string)
	count := 1
	for _, k := range imgs {
		img_map[count] = append(img_map[count], k)
		if len(img_map[count]) == helpers.ImageChunk {
			count += 1
		}
	}

	blogpost.Role = "gallery"
	blogpost.ImgLen = len(imgs)
	blogpost.Images = img_map[blogpost.CurrentPage]

	return blogpost
}

func RenderImage(path string, imgFile *os.File) *Blogpost {
	img := ReadMetadata(imgFile)
	img.ID = path
	blogpost.Metadata = img

	return blogpost
}

func RenderReplies(data *vocabulary.Collection) *Blogpost {
	blogpost.Replies = data

	return blogpost
}

func RenderTags(tags []string) *Blogpost {
	blogpost.Tags = tags

	return blogpost
}

func RenderTagFeed(ctx context.Context, data *vocabulary.OrderedCollection) *Blogpost {
	blogpost.Items = data
	cp := helpers.GetCurrentPage(ctx)
	blogpost.CurrentPage = cp

	return blogpost
}

func RenderFollowers(followers []string) *Blogpost {
	blogpost.Followers = followers

	return blogpost
}
