package views

import (
	"context"
	"encoding/json"
	"html/template"
	"os"
	"time"

	"github.com/russross/blackfriday/v2"
	"github.com/rwcarlsen/goexif/exif"
	"gitlab.com/kamee/picverse/activitypub/vocabulary"
	"gitlab.com/kamee/picverse/config"
	"gitlab.com/kamee/picverse/helpers"
	"gitlab.com/kamee/picverse/logger"
	"gitlab.com/kamee/picverse/services"
)

type Blogpost struct {
	Srv         *services.Server
	Body        template.HTML
	Webfinger   string
	CurrentID   string
	Motto       string
	Title       string
	Actor       map[string]interface{}
	Object      map[string]interface{}
	Items       *vocabulary.OrderedCollection
	Images      []string
	Replies     *vocabulary.Collection
	Tags        []string
	Followers   []string
	CurrentPage int
	PrevPage    int
	NextPage    int
	PrevImage   string
	NextImage   string
	*Metadata

	Role	string
	ImgLen int
}

type Metadata struct {
	ID                string
	ApertureValue     []string
	BrightnessValue   []string
	ColorSpace        []int
	DateTime          string
	ExposureMode      []int
	ExposureTime      []string
	FocalLength       []string
	ISOSpeedRatings   []int
	Make              string
	Model             string
	ShutterSpeedValue []string
	Software          string
	WhiteBalance      []int
}

type ImageFile struct {
	Path     string
	Metadata *Metadata
}

var blogpost *Blogpost

func (b *Blogpost) SetPages() int {
	if blogpost.CurrentPage == 1 {
		blogpost.PrevPage = blogpost.CurrentPage
	} else {
		blogpost.PrevPage = blogpost.CurrentPage - 1
	}

	blogpost.NextPage = blogpost.CurrentPage + 1

	
	if b.Role == "gallery" {
		if b.ImgLen <= blogpost.CurrentPage*helpers.ImageChunk {
			blogpost.NextPage = 1
		} else {
			blogpost.NextPage = blogpost.CurrentPage + 1
		}
	} else {
		if b.Items.TotalItems <= blogpost.CurrentPage*helpers.LimitItems {
			blogpost.NextPage = 1
		} else {
			blogpost.NextPage = blogpost.CurrentPage + 1
		}
	}


	return 1
}

func Template(ctx context.Context, srv *services.Server) *Blogpost {
	blogpost = &Blogpost{
		Srv:   srv,
		Motto: config.PodConfig().Motto,
	}

	return blogpost
}

func (b *Blogpost) Hostname() string {
	pod := config.PodConfig()
	return pod.Motto
}

func (b *Blogpost) Categories() []map[string]interface{} {
	actors, err := blogpost.Srv.User.FetchActors(context.Background())
	if err != nil {
		logger.Critical.Printf("[TEMPLATE]: Cannot fetch actors: %s", err)
	}

	return actors
}

func ParseContent(title, author string, object map[string]interface{}) *Blogpost {
	content := object["object"].(map[string]interface{})["content"].(string)
	output := template.HTML(blackfriday.Run([]byte(content)))
	blogpost.Body = output

	return blogpost
}

func (b *Blogpost) StylePath() string {
	wd, err := os.Getwd()
	if err != nil {
		logger.Warning.Printf("STYLE: cannot get path %s", err)
	}

	wd = wd + "/static/style.css"
	return wd
}

func (b *Blogpost) ActorAllPosts(ctx context.Context, actor string) *vocabulary.OrderedCollection {
	data, err := b.Srv.Outbox.FetchAllForActor(ctx, actor)
	if err != nil {
		logger.Critical.Printf("[TEMPLATE]: Cannot fetch actors: %s", err)
	}

	blogpost.Items = data

	return data
}

func (b *Blogpost) ActorPosts(ctx context.Context, actor string) *vocabulary.OrderedCollection {
	data, err := b.Srv.Outbox.FetchForActor(ctx, actor)
	if err != nil {
		logger.Critical.Printf("[TEMPLATE]: Cannot fetch actors: %s", err)
	}

	blogpost.Items = data

	return data
}

func (b *Blogpost) HTMLify(content string) template.HTML {
	output := template.HTML(blackfriday.Run([]byte(content)))
	return output
}

func (b *Blogpost) TimeFormater(rfcdate string) string {
	parseTime, err := time.Parse(time.RFC3339, rfcdate)
	if err != nil {
		logger.Info.Printf("[TEMPLATE]: cannot parse the time")
	}

	return parseTime.String()
}

func ReadMetadata(imgFile *os.File) *Metadata {
	var err error
	var metaData *exif.Exif
	var jsonByte []byte
	var metadata *Metadata

	metaData, err = exif.Decode(imgFile)
	if err != nil {
		logger.Error.Println(err)
		return new(Metadata)
	}

	jsonByte, err = metaData.MarshalJSON()
	if err != nil {
		logger.Error.Println(err)
		return new(Metadata)
	}

	json.Unmarshal(jsonByte, &metadata)

	return metadata
}

func (b *Blogpost) NameFormater(name string) string {
	return helpers.GetFromUrl(name)
}
