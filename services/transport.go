package services

import (
	"fmt"
	"gitlab.com/kamee/picverse/activitypub"
	"gitlab.com/kamee/picverse/logger"
)

func Send(followers []string, actor string, object []byte) {
	for _, follower := range followers {
		resp, err := activitypub.Send(follower, actor, object)
		if err != nil {
			err := fmt.Errorf(`Failed to send activity to pairs: %s,
                            follower id: %s`, err, follower)
			logger.Critical.Println(resp, err)
		}
	}
}
