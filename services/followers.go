package services

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/jackc/pgx/v5"

	"gitlab.com/kamee/picverse/activitypub"
	"gitlab.com/kamee/picverse/database"
	"gitlab.com/kamee/picverse/helpers"
	"gitlab.com/kamee/picverse/logger"
)

var _ Queries = (*Followers)(nil)

type Followers struct {
	DB *database.Repository
}

func (d *Followers) CreateTable(ctx context.Context) {
	err := beginTx(ctx, d.DB, func(tx pgx.Tx) error {
		err := d.DB.CreateFollowersTable(ctx, tx)
		if err != nil {
			return err
		}

		return err
	})

	if err != nil {
		logger.Critical.Fatal(err)
	}

	err = beginTx(ctx, d.DB, func(tx pgx.Tx) error {
		err := d.DB.CreateActorFollowersTable(ctx, tx)
		if err != nil {
			return err
		}

		return err
	})

	if err != nil {
		logger.Critical.Fatal(err)
	}
}

func (f *Followers) Autoaccept(ctx context.Context, activity map[string]interface{}) {
	delete(activity, "@context")
	remote_actor, local_actor, remote_inbox := helpers.ObjectParser(activity)
	actor := helpers.GetFromUrl(local_actor)

	accept := activitypub.CreateAccept(local_actor, remote_actor, activity)
	accept_byte, err := json.Marshal(accept)
	if err != nil {
		logger.Error.Println(err)
	}

	resp, err := activitypub.Send(remote_inbox, actor, accept_byte)
	if err != nil {
		err := fmt.Errorf("Failed to send activity to pairs: %s", err)
		logger.Critical.Println(err)
	}

	// TODO: test the response
	if resp == "200 OK" || resp == "202 Accepted" {
		f.Insert(ctx, actor, remote_inbox)
	}
}

func (f *Followers) Insert(ctx context.Context, actor, follower string) {
	logger.Info.Printf("Follow request from %s to %s", follower, actor)
	var err error
	var id int

	if !f.Exists(ctx, "actors", actor) {
		err = fmt.Errorf("actor %s not exists", actor)
		logger.Warning.Println(err)
	}

	err = beginTx(ctx, f.DB, func(tx pgx.Tx) error {
		err = f.DB.InsertFollower(ctx, tx, follower)
		if err != nil {
			return err
		}

		return err
	})

	if err != nil {
		err = fmt.Errorf("Failed to insert remote actor %s into db with error %s", actor, err)
		logger.Error.Println(err)
	}

	id, err = f.FetchID(ctx, follower)
	if err != nil {
		err = fmt.Errorf("Failed to get followers id %s with error %s", follower, err)
		logger.Error.Println(err)
	}

	err = beginTx(ctx, f.DB, func(tx pgx.Tx) error {
		err = f.DB.InsertActorFollowers(ctx, tx, actor, id)
		if err != nil {
			return err
		}

		return err
	})

	if err != nil {
		err = fmt.Errorf("Failed to insert actor's follower %s into db with %s", follower, err)
		logger.Error.Println(err)
	}
}

func (f *Followers) Delete(ctx context.Context, activity map[string]interface{}) (err error) {
	remote_actor, err := f.FetchID(ctx, activity["actor"].(string)+"/inbox")
	if err != nil || remote_actor == -1 {
		err = fmt.Errorf("Failed to find remote actor %s", err)
		logger.Error.Println(err)
	}

	_activity, err := json.Marshal(activity)
	if err != nil {
		err = fmt.Errorf("Failed to marshal activity object: %s", err)
		logger.Error.Println(err)
	}

	local_actor, err := helpers.Get(_activity, "object")
	if err != nil {
		err = fmt.Errorf("Failed to find local actor: %s", err)
		logger.Error.Println(err)
	}

	err = beginTx(ctx, f.DB, func(tx pgx.Tx) error {
		err = f.DB.DeleteActorFollower(ctx, tx, local_actor, remote_actor)
		if err != nil {
			return err
		}

		return err
	})

	if err != nil {
		err = fmt.Errorf("Failed to delete follower from db, caused by: %s", err)
		logger.Error.Println(err)
	}

	return err
}

func (f *Followers) FetchID(ctx context.Context, actor string) (id int, err error) {
	err = beginTx(ctx, f.DB, func(tx pgx.Tx) error {
		id, err = f.DB.FetchID(ctx, tx, actor)
		if err != nil {
			return err
		}

		return err
	})

	if err != nil {
		err = fmt.Errorf("Failed to get ID for the actor %s with %s", actor, err)
		logger.Warning.Println(err)
		return -1, err
	}

	return id, err
}

func (f *Followers) Fetch(ctx context.Context, actor string) (object []string, err error) {
	err = beginTx(ctx, f.DB, func(tx pgx.Tx) error {
		object, err = f.DB.FetchFollowers(ctx, tx, actor)
		if err != nil {
			return err
		}

		return err
	})

	if err != nil {
		err = fmt.Errorf("Failed to insert remote actor into db %s with error %s", actor, err)
		logger.Error.Println(err)
		return nil, err
	}

	return
}

func (f *Followers) Exists(ctx context.Context, _tbl, _val string) bool {
	exists := false
	err := beginTx(ctx, f.DB, func(tx pgx.Tx) (err error) {
		exists, err = f.DB.Exists(ctx, tx, _tbl, _val)
		if err != nil {
			return err
		}

		return nil
	})

	if err != nil {
		err = fmt.Errorf("Failed to find the follower %s in table %s with error %s", _val, _tbl, err)
		logger.Error.Println(err)
	}

	return exists
}
