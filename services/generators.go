package services

import (
	"bufio"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"strings"

	"gitlab.com/kamee/picverse/activitypub"
	"gitlab.com/kamee/picverse/helpers"
	"gitlab.com/kamee/picverse/logger"

	"github.com/russross/blackfriday/v2"
)

func (srv *Server) NewActor(ctx context.Context) {
	var actorid, username, summary, role string

	fmt.Println("keep in mind, you cannot change actor id, but u can change username. lets start")
	fmt.Print("actor id is: ")
	fmt.Scan(&actorid)
	fmt.Print("actor's username is: ")
	fmt.Scan(&username)

	// TODO: describe gallery
	fmt.Println("in gallery mode you can see photo grids")
	fmt.Print("do you want to enable gallery mode?|[y/N]")
	fmt.Scan(&role)

	if role == "y" {
		role = "gallery"
	} else {
		role = "person"
	}

	fmt.Print("description for the actor: ")
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	err := scanner.Err()
	if err != nil {
		logger.Warning.Println(err)
	}
	summary = scanner.Text()

	actor := activitypub.APActor(actorid, username, summary)
	_actor, err := json.Marshal(actor)
	if err != nil {
		err = fmt.Errorf("json marshaling error: %s", err.Error())
		logger.Critical.Println(err)
	}

	srv.User.Insert(ctx, actorid, _actor, role)
}

func (srv *Server) CreatePhotoList(ctx context.Context) {
	var dir string
	fmt.Print("if there is an attachment/image please insert its dir name: ")
	fmt.Scan(&dir)

	path := fmt.Sprintf("images/%s", dir)

	entries, err := os.ReadDir(path)
	if err != nil {
		logger.Critical.Println("Cannot read file: ", err)
	}

	var s strings.Builder
	for _, e := range entries {
		img := fmt.Sprintf("/%s/%s", path, e.Name())
		img_line := fmt.Sprintf("![](%s)\n", img)
		s.WriteString(img_line)
	}

	f, err := os.Create("image_list.md")

	if err != nil {
		logger.Error.Println(err)
	}

	defer f.Close()

	_, err = f.WriteString(s.String())

	if err != nil {
		logger.Error.Println(err)
	}
}

func (srv *Server) NewNote(ctx context.Context) {
	var filename, content, title, author, note_type, dir, img_content string
	var tags, imgs []string
	var err error

	fmt.Print("file name is: ")
	fmt.Scan(&filename)
	content, err = helpers.LoadContent(filename)
	if errors.Is(err, os.ErrNotExist) {
		err = fmt.Errorf("file not found, please create file and content to continue: %s", err)
		logger.Critical.Println(err)
	}

	fmt.Print("input title: ")
	title_scan := bufio.NewScanner(os.Stdin)
	title_scan.Scan()
	err = title_scan.Err()
	if err != nil {
		logger.Error.Println(err)
	}

	title = title_scan.Text()
	if title == "" {
		title = "..."
	}

	fmt.Print("who is the author of this post ")
	fmt.Scan(&author)

	fmt.Print("if there is an attachment/image please insert its dir name: ")
	dir_scan := bufio.NewScanner(os.Stdin)
	dir_scan.Scan()
	err = dir_scan.Err()
	if err != nil {
		logger.Error.Println(err)
	}

	dir = dir_scan.Text()

	if dir != "" {
		path := fmt.Sprintf("images/%s", dir)

		entries, err := os.ReadDir(path)
		if err != nil {
			logger.Critical.Println("Cannot read file: ", err)
		}

		for _, e := range entries {
			img := fmt.Sprintf("%s/%s", path, e.Name())
			imgs = append(imgs, img)
		}

		fmt.Print("if you want, you can describe your images: ")
		img_desc := bufio.NewScanner(os.Stdin)
		img_desc.Scan()
		err = img_desc.Err()
		if err != nil {
			logger.Error.Println(err)
		}
		img_content = img_desc.Text()
	}

	fmt.Print("if you want, you can enter tags: ")
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	err = scanner.Err()
	if err != nil {
		logger.Error.Println(err)
	}

	inp_tags := scanner.Text()
	if inp_tags != "" {
		tags = strings.Split(inp_tags, ", ")
	}

	content, vocab_tags := helpers.TagBuilder(tags, content)

	fmt.Print("is this a draft, plase type post or draft ")
	fmt.Scan(&note_type)

	output := blackfriday.Run([]byte(content))
	note := activitypub.CreateNote(author, string(output), title, vocab_tags, "text/html", imgs, img_content)
	/*
		if len(imgs) != 0 {
			note = activitypub.CreateNoteAttachment(author, string(output), title, vocab_tags, imgs, img_content, "text/html")
		} else {
			note = activitypub.CreateNote(author, string(output), title, vocab_tags, "text/html")
		}
	*/

	_note, err := json.Marshal(note)
	if err != nil {
		err = fmt.Errorf("json marshaling error: %s", err.Error())
		logger.Error.Println(err)
	}

	followers, err := srv.Follower.Fetch(ctx, author)
	logger.Info.Println("Fetching followers")
	if err != nil {
		err = fmt.Errorf("fetching: %s", err.Error())
		logger.Critical.Println(err)
	}

	switch note_type {
	case "post":
		srv.Outbox.Insert(ctx, title, _note, tags, followers)
	case "draft":
		srv.Draft.Insert(ctx, title, _note, tags)
	}
}

func (srv *Server) Fetch(ctx context.Context, _tbl string) {
	switch _tbl {
	case "outbox":
		helpers.MapPrinter(srv.Outbox.Fetch(ctx))
	case "drafts":
		helpers.MapPrinter(srv.Draft.Fetch(ctx))
	case "actors":
		helpers.MapPrinter(srv.User.Fetch(ctx))
	}
}

func (srv *Server) Delete(ctx context.Context, _tbl string) {
	var _val string
	fmt.Print("delete whom?, please insert the activity id: ")
	fmt.Scan(&_val)

	switch _tbl {
	case "actor":
		srv.User.Delete(ctx, _val)
	case "note":
		srv.Outbox.Delete(ctx, _val)
	case "draft":
		srv.Draft.Delete(ctx, _val)
	}
}

func (srv *Server) SetSummary(ctx context.Context, actorid string) {
	var summary string
	fmt.Print("new description for the actor: ")
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	err := scanner.Err()
	if err != nil {
		logger.Error.Println(err)
	}
	summary = scanner.Text()
	srv.User.Update(ctx, "summary", actorid, summary)
}

// TODO: check if there is necessity to fetch & send data from 'outboxes' table
func (srv *Server) UpdateDraft(ctx context.Context, _id string) {
	actor, object, err := srv.Draft.FetchColumns(ctx, _id)
	_note, err := json.Marshal(object)
	if err != nil {
		logger.Error.Println(err)
	}

	followers, err := srv.Follower.Fetch(ctx, actor)
	if err != nil {
		logger.Error.Println(err)
	}

	Send(followers, actor, _note)

	srv.Draft.Update(ctx, _id)
}

func (srv *Server) Update(ctx context.Context, _tbl string) {
	var _val, _new_val string
	fmt.Print("update whom? please insert the activity id: ")
	fmt.Scan(&_val)

	switch _tbl {
	case "draft":
		srv.UpdateDraft(ctx, _val)
	case "actor":
		fmt.Print("insert new username: ")
		fmt.Scan(&_new_val)
		srv.User.Update(ctx, "username", _val, _new_val)
	case "description":
		srv.SetSummary(ctx, _val)
	}
}

func (srv *Server) Load(ctx context.Context, _tbl string) {
	var _val string
	var content []map[string]string
	fmt.Print("input title: ")
	fmt.Scan(&_val)

	switch _tbl {
	case "draft":
		content, _ = srv.Draft.FetchByTitle(ctx, _val)
	case "note":
		content, _ = srv.Outbox.FetchByTitle(ctx, _val)
	}

	for _, el := range content {
		for id, text := range el {
			fmt.Println(text)
			fmt.Println("id is: ", id)
		}
	}
}
