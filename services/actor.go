package services

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v5"

	"gitlab.com/kamee/picverse/database"
	"gitlab.com/kamee/picverse/logger"
)

var _ Queries = (*Actor)(nil)

type Actor struct {
	DB *database.Repository
}

func (a *Actor) CreateTable(ctx context.Context) {
	err := beginTx(ctx, a.DB, func(tx pgx.Tx) error {
		err := a.DB.CreateActorsTable(ctx, tx)
		if err != nil {
			return err
		}

		return err
	})

	if err != nil {
		logger.Critical.Fatal(err)
	}
}

func (a *Actor) CreateIndex(ctx context.Context) {
	err := beginTx(ctx, a.DB, func(tx pgx.Tx) error {
		err := a.DB.CreateActorIndex(ctx, tx)
		if err != nil {
			return err
		}

		return err
	})

	if err != nil {
		logger.Critical.Fatal(err)
	}
}

func (a *Actor) Insert(ctx context.Context, actorid string, object []byte, role string) {
	err := beginTx(ctx, a.DB, func(tx pgx.Tx) (err error) {
		if a.Exists(ctx, "actors", actorid) {
			exists := fmt.Sprintf("actor %s already exists", actorid)
			logger.Warning.Println(exists)
		} else {
			err = a.DB.InsertActor(ctx, tx, actorid, object, role)
			if err != nil {
				err = fmt.Errorf("Failed to insert actor into db, caused by: %s", err)
				logger.Critical.Println(err)
				return err
			}
		}

		return err
	})

	if err != nil {
		logger.Error.Println(err)
	}

	logger.Info.Println("Created new actor: ", actorid)
}

func (a *Actor) Fetch(ctx context.Context) (object []map[interface{}]interface{}) {
	err := beginTx(ctx, a.DB, func(tx pgx.Tx) (err error) {
		object, err = a.DB.FetchActors(ctx, tx)
		if err != nil {
			return err
		}

		return err
	})

	if err != nil {
		err = fmt.Errorf("Failed to fetch actors: %s for actor", err)
		logger.Error.Println(err)
		return nil
	}

	return object
}

func (a *Actor) FetchByID(ctx context.Context, id string) (object map[string]interface{}, role string, err error) {
	logger.Info.Printf("Searching for actor %s", id)
	err = beginTx(ctx, a.DB, func(tx pgx.Tx) error {
		object, role, err = a.DB.FetchByID(ctx, tx, "actors", id)
		if err != nil {
			return err
		}

		return err
	})

	if err != nil {
		logger.Error.Printf("Searching actor %s, failed with error: %s", id, err)
		return nil, "", err
	}

	return object, role, err
}

// TODO: naming convention
func (a *Actor) FetchActors(ctx context.Context) (obj []map[string]interface{}, err error) {
	err = beginTx(ctx, a.DB, func(tx pgx.Tx) error {
		obj, err = a.DB.SelectActors(ctx, tx)
		if err != nil {
			return err
		}

		return err
	})

	if err != nil {
		err = fmt.Errorf("Failed to fetch data: %s", err)
		logger.Error.Println(err)
		return nil, err
	}

	return obj, err
}

func (a *Actor) Update(ctx context.Context, _clm, actor, new_username string) {
	err := beginTx(ctx, a.DB, func(tx pgx.Tx) error {
		err := a.DB.UpdateActor(ctx, tx, _clm, actor, new_username)
		if err != nil {
			return err
		}

		return err
	})

	if err != nil {
		err = fmt.Errorf("Failed to update actor's username for actor %s with err: %s", actor, err)
		logger.Error.Println(err)
	}

	logger.Info.Printf("Actor's %s is changed from %s to %s ", _clm, actor, new_username)
}

func (a *Actor) Exists(ctx context.Context, _tbl, _val string) bool {
	exists := false
	err := beginTx(ctx, a.DB, func(tx pgx.Tx) (err error) {
		exists, err = a.DB.Exists(ctx, tx, _tbl, _val)
		if err != nil {
			return err
		}

		return nil
	})

	if err != nil {
		err = fmt.Errorf("Failed to find the actor: %s", err)
		logger.Error.Println(err)
	}

	return exists
}

func (a *Actor) Delete(ctx context.Context, actor string) {
	err := beginTx(ctx, a.DB, func(tx pgx.Tx) (err error) {
		err = a.DB.Delete(ctx, tx, "actor", actor)
		if err != nil {
			return err
		}

		return err
	})

	if err != nil {
		err = fmt.Errorf("Failed to delete the actor: %s", err)
		logger.Error.Println(err)
	}

	logger.Info.Printf("Delete actor %s.", actor)
}
