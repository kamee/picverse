package services

import (
	"time"
	"encoding/xml"
	"os"
	"context"
	"path"
	"github.com/robfig/cron"
	"github.com/gorilla/feeds"
	"gitlab.com/kamee/picverse/config"
)

func (srv *Server) GenerateAtomFeed(ctx context.Context) (string, error) {
	data, err := srv.Outbox.FetchAll(ctx)
	if err != nil {
		return "", err
	}
	host := config.PodConfig().Host
	author := config.PodConfig().Author
	email := config.PodConfig().Email

	feed := &feeds.Feed{
        	Title:       config.PodConfig().Motto,
		Link:        &feeds.Link{Href: host},
        	Description: config.PodConfig().Message,
		Author:      &feeds.Author{Name: author, Email: email},
		Created:     time.Now(),
	}

	feed.Items = []*feeds.Item{}
	for _, value := range data.OrderedItems {
		item := &feeds.Item{
			Title: value["object"].(map[string]interface{})["summary"].(string),
			Author: &feeds.Author{Name: path.Base(value["actor"].(string))},
			Content: value["object"].(map[string]interface{})["content"].(string),
			Created: parseTime(value["object"].(map[string]interface{})["published"].(string)),
		}
		feed.Items = append(feed.Items, item)
	}

	atom, err := feed.ToAtom()
    	if err != nil {
		return "", err
    	}

	output, err := xml.MarshalIndent(feed, "", "  ")
	if err != nil {
		return "", err
	}

	file, err := os.Create("atom.xml")
	if err != nil {
		return "", err
	}
	defer file.Close()

	file.WriteString(xml.Header)
	file.Write(output)

	return atom, nil
}

func (srv *Server) UpdateFeed() {
	c := cron.New()
	c.AddFunc("0 * * * *", func() {
		srv.GenerateAtomFeed(context.Background())
	})
	c.Start()
}

func parseTime(timeStr string) time.Time {
	layout := time.RFC3339
	parsedTime, err := time.Parse(layout, timeStr)
	if err != nil {
		return time.Now()
	}

	return parsedTime
}
