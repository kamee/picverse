package services

import (
	"context"
	"github.com/jackc/pgx/v5"

	"gitlab.com/kamee/picverse/database"
	"gitlab.com/kamee/picverse/logger"
)

type Server struct {
	User     *Actor
	Outbox   *Outboxes
	Draft    *Drafts
	Follower *Followers
	Inbox    *Inboxes
}

func NewServer(a *Actor, o *Outboxes, d *Drafts, f *Followers, i *Inboxes) *Server {
	return &Server{
		User:     a,
		Outbox:   o,
		Draft:    d,
		Follower: f,
		Inbox:    i,
	}
}

// TODO: check the correctness
func beginTx(ctx context.Context, db *database.Repository, fn func(tx pgx.Tx) error) error {
	tx, err := db.Conn.BeginTx(ctx, pgx.TxOptions{})
	select {
	case <-ctx.Done():
		logger.Warning.Println(ctx.Err())
		return ctx.Err()
	default:
	}

	defer tx.Rollback(ctx)

	if err != nil {
		logger.Critical.Println(err)
		return err
	}

	if err = fn(tx); err != nil {
		logger.Critical.Println(err)
		return err
	}

	return tx.Commit(ctx)
}

func (srv *Server) CreateTables(ctx context.Context, queries []Queries) {
	for _, query := range queries {
		query.CreateTable(ctx)
	}
}

func (srv *Server) CreateIndex(ctx context.Context) {
	srv.Inbox.CreateIndex(ctx)
	srv.User.CreateIndex(ctx)
}
