package services

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v5"

	"gitlab.com/kamee/picverse/database"
	"gitlab.com/kamee/picverse/helpers"
	"gitlab.com/kamee/picverse/logger"
)

var _ Queries = (*Drafts)(nil)

type Drafts struct {
	DB *database.Repository
}

func (d *Drafts) CreateTable(ctx context.Context) {
	err := beginTx(ctx, d.DB, func(tx pgx.Tx) error {
		err := d.DB.CreateDraftsTable(ctx, tx)
		if err != nil {
			return err
		}

		return err
	})

	if err != nil {
		logger.Critical.Fatal(err)
	}
}

func (d *Drafts) Insert(ctx context.Context, title string, object []byte, tags []string) {
	logger.Info.Println("Creating new draft %s.", title)
	actor, err := helpers.Get(object, "actor")
	if err != nil {
		err = fmt.Errorf("Wrong actor format: %s", err)
		logger.Error.Println(err)
	}

	if !d.Exists(ctx, "actors", actor) {
		err = fmt.Errorf("actor %s not exists, please insert existing username", actor)
		logger.Warning.Println(err)
	}

	id, err := helpers.Get(object, "note")
	if err != nil {
		err = fmt.Errorf("Wrong activity note format: %s", err)
		logger.Warning.Println(err)
	}

	if d.Exists(ctx, "drafts", id) {
		id = helpers.UniqueID(id, title)
		logger.Info.Println("Created new unique id for the draft: ", id)
	}

	err = beginTx(ctx, d.DB, func(tx pgx.Tx) error {
		err := d.DB.InsertDrafts(ctx, tx, actor, id, title, object, tags)
		if err != nil {
			return err
		}

		return err
	})

	if err != nil {
		err = fmt.Errorf("Failed to insert draft into db, caused by: %s", err)
		logger.Error.Println(err)
	}
}

func (d *Drafts) Update(ctx context.Context, activity_id string) {
	err := beginTx(ctx, d.DB, func(tx pgx.Tx) error {
		err := d.DB.UpdateDraft(ctx, tx, activity_id)
		if err != nil {
			return err
		}

		return err
	})

	if err != nil {
		err = fmt.Errorf("Failed to update note, err: %s", err)
		logger.Error.Println(err)
	}

	logger.Info.Println("Draft is updated: ", activity_id)
}

func (d *Drafts) Fetch(ctx context.Context) (object []map[interface{}]interface{}) {
	var err error
	err = beginTx(ctx, d.DB, func(tx pgx.Tx) error {
		object, err = d.DB.Fetch(ctx, tx, "drafts")
		if err != nil {
			return err
		}

		return err
	})

	if err != nil {
		err = fmt.Errorf("Failed to fetch data: %s", err)
		logger.Warning.Println(err)
		return nil
	}

	return object
}

func (d *Drafts) FetchByTitle(ctx context.Context, title string) (content []map[string]string, err error) {
	err = beginTx(ctx, d.DB, func(tx pgx.Tx) error {
		content, err = d.DB.FetchByTitle(ctx, tx, "drafts", title)
		if err != nil {
			return err
		}

		return err
	})

	if err != nil {
		err = fmt.Errorf("Failed to fetch draft %s with error: %s", title, err)
		logger.Warning.Println(err)
		return nil, err
	}

	return content, err
}

func (d *Drafts) FetchByID(ctx context.Context, id string) (object map[string]interface{}, err error) {
	err = beginTx(ctx, d.DB, func(tx pgx.Tx) error {
		object, _, err = d.DB.FetchByID(ctx, tx, "drafts", id)
		if err != nil {
			return err
		}

		return err
	})

	if err != nil {
		err = fmt.Errorf("Failed to fetch draft %s with error %s", id, err)
		logger.Warning.Println(err)
		return nil, err
	}

	return object, err
}

func (d *Drafts) FetchColumns(ctx context.Context, activity_id string) (actor string, object map[string]interface{}, err error) {
	err = beginTx(ctx, d.DB, func(tx pgx.Tx) error {
		actor, object, err = d.DB.FetchDraftData(ctx, tx, activity_id)
		if err != nil {
			return err
		}

		return err
	})

	if err != nil {
		err = fmt.Errorf("Failed to fetch draft %s with error %s", activity_id, err)
		logger.Warning.Println(err)
		return "", nil, err
	}

	return
}

func (d *Drafts) Exists(ctx context.Context, _tbl, _val string) bool {
	exists := false
	err := beginTx(ctx, d.DB, func(tx pgx.Tx) (err error) {
		exists, err = d.DB.Exists(ctx, tx, _tbl, _val)
		if err != nil {
			return err
		}
		return err
	})

	if err != nil {
		err = fmt.Errorf("Failed to find the draft %s: %s", _val, err)
		logger.Warning.Println(err)
	}

	return exists
}

func (d *Drafts) Delete(ctx context.Context, title string) {
	err := beginTx(ctx, d.DB, func(tx pgx.Tx) (err error) {
		err = d.DB.Delete(ctx, tx, "draft", title)
		if err != nil {
			return err
		}
		return err
	})

	if err != nil {
		err = fmt.Errorf("Failed to delete the draft %s with error %s", title, err)
		logger.Warning.Println(err)
	}

	logger.Info.Printf("Deleting draft %s.", title)
}
