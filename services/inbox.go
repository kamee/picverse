package services

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/jackc/pgx/v5"
	"strings"

	"gitlab.com/kamee/picverse/activitypub"
	"gitlab.com/kamee/picverse/activitypub/vocabulary"
	"gitlab.com/kamee/picverse/database"
	"gitlab.com/kamee/picverse/helpers"
	"gitlab.com/kamee/picverse/logger"
)

type Inboxes struct {
	DB *database.Repository
}

func (i *Inboxes) CreateTable(ctx context.Context) {
	err := beginTx(ctx, i.DB, func(tx pgx.Tx) error {
		err := i.DB.CreateRepliesTable(ctx, tx)
		if err != nil {
			return err
		}

		return err
	})

	if err != nil {
		logger.Critical.Fatal(err)
	}
}

func (i *Inboxes) CreateIndex(ctx context.Context) {
	err := beginTx(ctx, i.DB, func(tx pgx.Tx) error {
		err := i.DB.CreateNoteIDindex(ctx, tx)
		if err != nil {
			return err
		}

		return err
	})

	if err != nil {
		logger.Critical.Println(err)
	}
}

func (i *Inboxes) Insert(ctx context.Context, note_id string, activity []byte) {
	var err error
	reply_id := helpers.GetFromUrl(vocabulary.ID())

	if i.Exists(ctx, "replies", reply_id) {
		reply_id = helpers.UniqueID(reply_id, vocabulary.ID())
		logger.Info.Println("Created new unique id for the reply: ", reply_id)
	}

	if !i.Exists(ctx, "outboxes", note_id) {
		err = fmt.Errorf("Reply to nonexistence: %s", activity)
		logger.Info.Println(err)
	}

	err = beginTx(ctx, i.DB, func(tx pgx.Tx) error {
		err = i.DB.InsertReply(ctx, tx, reply_id, note_id, activity)
		if err != nil {
			return err
		}

		return err
	})

	if err != nil {
		err = fmt.Errorf("Failed to insert reply into db %s with error %s", note_id, err)
		logger.Error.Println(err)
	}
}

func (i *Inboxes) Fetch(ctx context.Context, note_id string) (collection *vocabulary.Collection, err error) {
	var items_count int
	var object []map[string]interface{}
	err = beginTx(ctx, i.DB, func(tx pgx.Tx) error {
		object, items_count, err = i.DB.FetchReplies(ctx, tx, note_id)
		if err != nil {
			return err
		}
		return err
	})

	if err != nil {
		err = fmt.Errorf("Failed to fetch reply from db %s with error %s", note_id, err)
		logger.Warning.Println(err)
		return nil, err
	}

	collection = activitypub.CreateCollection(items_count, object)

	return collection, err
}

func (i *Inboxes) Exists(ctx context.Context, _tbl string, _val string) bool {
	exists := false
	err := beginTx(ctx, i.DB, func(tx pgx.Tx) (err error) {
		exists, err = i.DB.Exists(ctx, tx, _tbl, _val)
		if err != nil {
			return err
		}

		return err
	})

	if err != nil {
		err = fmt.Errorf("Failed to find reply %s in table %s with error %s", _val, _tbl, err)
		logger.Error.Println(err)
	}

	return exists
}

func (i *Inboxes) Process(ctx context.Context, activity map[string]interface{}) {
	var _addr string
	visibility := activity["to"]

	switch visibility.(type) {
	case []interface{}:
		_visibility := visibility.([]interface{})
		s := make([]string, len(_visibility))
		for i, v := range _visibility {
			s[i] = fmt.Sprint(v)
		}
		_addr = strings.Join(s, ", ")
	case string:
		_addr = visibility.(string)
	}

	if strings.Contains(_addr, vocabulary.PublicActivity) {
		_object := activity["object"]
		obj_byte, err := json.Marshal(_object)
		note_id, err := helpers.Get(obj_byte, "reply")
		if err != nil {
			err = fmt.Errorf("Failed to get note id: %s", err)
			logger.Warning.Println(err)
		}

		i.Insert(ctx, note_id, obj_byte)
	}
}
