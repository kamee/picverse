package services

import (
	"context"
)

type Queries interface {
	CreateTable(ctx context.Context)
}
