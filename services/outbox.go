package services

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v5"

	"gitlab.com/kamee/picverse/activitypub"
	"gitlab.com/kamee/picverse/activitypub/vocabulary"
	"gitlab.com/kamee/picverse/database"
	"gitlab.com/kamee/picverse/helpers"
	"gitlab.com/kamee/picverse/logger"
)

var _ Queries = (*Outboxes)(nil)

type Outboxes struct {
	DB *database.Repository
}

func (o *Outboxes) CreateTable(ctx context.Context) {
	err := beginTx(ctx, o.DB, func(tx pgx.Tx) error {
		err := o.DB.CreateOutboxesTable(ctx, tx)
		if err != nil {
			return err
		}

		return err
	})

	if err != nil {
		logger.Critical.Fatal(err)
	}
}

func (o *Outboxes) Insert(ctx context.Context, title string, object []byte, tags, followers []string) {
	logger.Info.Printf("Creating new note %s.", title)
	actor, err := helpers.Get(object, "actor")
	if err != nil {
		err = fmt.Errorf("Wrong activity note format: %s", err)
		logger.Error.Println(err)
	}

	if !o.Exists(ctx, "actors", actor) {
		err = fmt.Errorf("actor not exists, please insert existing username")
		logger.Error.Println(err)
	}

	id, err := helpers.Get(object, "note")
	if err != nil {
		err = fmt.Errorf("Wrong activity note format: %s", err)
		logger.Error.Println(err)
	}

	if o.Exists(ctx, "outboxes", id) {
		id = helpers.UniqueID(id, title)
		logger.Info.Println("Created new unique id for the draft: ", id)
	}

	err = beginTx(ctx, o.DB, func(tx pgx.Tx) error {
		_, err = o.DB.InsertOutbox(ctx, tx, actor, id, title, object, tags)
		if err != nil {
			return err
		}

		return err
	})

	if err != nil {
		err = fmt.Errorf("Failed to insert note into db, caused by: %s", err)
		logger.Critical.Println(err)
	}

	Send(followers, actor, object)
}

func (o *Outboxes) Fetch(ctx context.Context) (object []map[interface{}]interface{}) {
	var err error
	err = beginTx(ctx, o.DB, func(tx pgx.Tx) error {
		object, err = o.DB.Fetch(ctx, tx, "outboxes")
		if err != nil {
			return err
		}

		return err
	})

	if err != nil {
		err = fmt.Errorf("Failed to fetch data: %s", err)
		logger.Error.Println(err)
		return nil
	}

	return object
}

func (o *Outboxes) FetchAll(ctx context.Context) (collection *vocabulary.OrderedCollection, err error) {
	var items_count int
	var offset, limit int
	var object []map[string]interface{}
	val := ctx.Value("page")
	if val == nil {
		offset, limit = helpers.GetPageLimit("1")
	} else {
		offset, limit = helpers.GetPageLimit(val.(string))
	}

	err = beginTx(ctx, o.DB, func(tx pgx.Tx) error {
		object, err = o.DB.FetchAll(ctx, tx, offset, limit)
		if err != nil {
			return err
		}

		return err
	})

	err = beginTx(ctx, o.DB, func(tx pgx.Tx) error {
		items_count, err = o.DB.GetCountAll(ctx, tx, "outboxes")
		if err != nil {
			return err
		}

		return err
	})

	if err != nil {
		err = fmt.Errorf("Failed to fetch data: %s", err)
		logger.Error.Println(err)
		return nil, err
	}

	collection = activitypub.CreateOrderedCollection(items_count, object)

	return collection, err
}

func (o *Outboxes) FetchAllForActor(ctx context.Context, actor string) (collection *vocabulary.OrderedCollection, err error) {
	var items_count int
	var object []map[string]interface{}
//	page := ctx.Value("page").(string)

	err = beginTx(ctx, o.DB, func(tx pgx.Tx) error {
		object, err = o.DB.FetchAllForActor(ctx, tx, actor)
		if err != nil {
			return err
		}

		return err
	})

	err = beginTx(ctx, o.DB, func(tx pgx.Tx) error {
		items_count, err = o.DB.GetCountActor(ctx, tx, actor)
		if err != nil {
			return err
		}

		return err
	})

	if err != nil {
		err = fmt.Errorf("Failed to actor %s postes with error %s", actor, err)
		logger.Error.Println(err)
		return nil, err
	}

	ordered_collection := activitypub.CreateOrderedCollection(items_count, object)

	return ordered_collection, err
}
func (o *Outboxes) FetchForActor(ctx context.Context, actor string) (collection *vocabulary.OrderedCollection, err error) {
	var items_count int
	var object []map[string]interface{}
	page := ctx.Value("page").(string)
	offset, limit := helpers.GetPageLimit(page)

	err = beginTx(ctx, o.DB, func(tx pgx.Tx) error {
		object, err = o.DB.FetchForActor(ctx, tx, actor, offset, limit)
		if err != nil {
			return err
		}

		return err
	})

	err = beginTx(ctx, o.DB, func(tx pgx.Tx) error {
		items_count, err = o.DB.GetCountActor(ctx, tx, actor)
		if err != nil {
			return err
		}

		return err
	})

	if err != nil {
		err = fmt.Errorf("Failed to actor %s postes with error %s", actor, err)
		logger.Error.Println(err)
		return nil, err
	}

	ordered_collection := activitypub.CreateOrderedCollection(items_count, object)

	return ordered_collection, err
}

func (o *Outboxes) FetchByTitle(ctx context.Context, title string) (content []map[string]string, err error) {
	logger.Info.Printf("Searching note %s\n", title)
	err = beginTx(ctx, o.DB, func(tx pgx.Tx) error {
		content, err = o.DB.FetchByTitle(ctx, tx, "outboxes", title)
		if err != nil {
			return err
		}

		return err
	})

	if err != nil {
		err = fmt.Errorf("Failed to fetch note %s with error %s", title, err)
		logger.Warning.Println(err)
		return nil, err
	}

	return content, err
}

func (o *Outboxes) FetchByID(ctx context.Context, id string) (object map[string]interface{},
	actor, title string,
	tags []string,
	err error) {

	logger.Info.Printf("Searching note %s\n", id)
	err = beginTx(ctx, o.DB, func(tx pgx.Tx) error {
		object, actor, title, tags, err = o.DB.FetchNoteByID(ctx, tx, id)
		if err != nil {
			return err
		}

		return err
	})

	if err != nil {
		err = fmt.Errorf("Failed to fetch note %s with error %s", id, err)
		logger.Error.Println(err)
		return
	}

	return object, actor, title, tags, err
}

func (o *Outboxes) FetchByTag(ctx context.Context, tag string) (collection *vocabulary.OrderedCollection, err error) {
	page := ctx.Value("page").(string)
	var object []map[string]interface{}
	var items_count int

	offset, limit := helpers.GetPageLimit(page)
	err = beginTx(ctx, o.DB, func(tx pgx.Tx) error {
		object, items_count, err = o.DB.FetchByTag(ctx, tx, tag, offset, limit)
		if err != nil {
			return err
		}

		return err
	})

	if err != nil {
		err = fmt.Errorf("Failed to fetch data for tag %s with error %s", tag, err)
		logger.Error.Println(err)
		return nil, err
	}

	collection = activitypub.CreateOrderedCollection(items_count, object)

	return
}

func (o *Outboxes) FetchTags(ctx context.Context) (tags []string, err error) {
	err = beginTx(ctx, o.DB, func(tx pgx.Tx) error {
		tags, err = o.DB.FetchTags(ctx, tx)
		if err != nil {
			return err
		}

		return err
	})

	if err != nil {
		err = fmt.Errorf("Failed to fetch data: %s", err)
		logger.Error.Println(err)
		return nil, err
	}

	tags = helpers.GetUniqueTags(tags)

	return
}

func (o *Outboxes) Exists(ctx context.Context, _tbl, _val string) bool {
	exists := false
	err := beginTx(ctx, o.DB, func(tx pgx.Tx) (err error) {
		exists, err = o.DB.Exists(ctx, tx, _tbl, _val)
		if err != nil {
			return err
		}
		return nil
	})

	if err != nil {
		err = fmt.Errorf("Failed to find data %s in table with error %s", _val, _tbl, err)
		logger.Error.Println(err)
		return exists
	}

	return exists
}

func (o *Outboxes) Delete(ctx context.Context, title string) {
	logger.Info.Printf("Delete note: %s.", title)
	err := beginTx(ctx, o.DB, func(tx pgx.Tx) (err error) {
		err = o.DB.Delete(ctx, tx, "note", title)
		if err != nil {
			return err
		}

		return err
	})

	if err != nil {
		err = fmt.Errorf("Failed to delete the actor: %s", err)
		logger.Warning.Println(err)
	}

	logger.Info.Printf("Deleting note %s", title)
}

