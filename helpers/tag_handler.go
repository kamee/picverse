package helpers

import (
	"fmt"
	"gitlab.com/kamee/picverse/activitypub/vocabulary"
	"strings"
)

func build_html_tag(tagid, tag string) string {
	html_tag := fmt.Sprintf(`<span class="tags"><a class="hashtag" href="%s" rel="tag ugc">%s</a></span>`, tagid, tag)

	return html_tag
}

func TagBuilder(tags []string, content string) (string, []*vocabulary.Tags) {
	var sb strings.Builder
	sb.WriteString(content)
	var vocab_tags []*vocabulary.Tags
	for i, elem := range tags {
		if elem != "" {
			tagid := vocabulary.TagID(elem)
			elem = "#" + elem
			tag := build_html_tag(tagid, elem)
			if i > 0 {
				sb.WriteString(" ") // Add space between tags
			}
			sb.WriteString(tag)
			_tag := &vocabulary.Tags{
				Href: tagid,
				Name: elem,
				Type: vocabulary.HashtagType,
			}
			vocab_tags = append(vocab_tags, _tag)
		}
	}

	return sb.String(), vocab_tags
}

func GetUniqueTags(tags []string) []string {
	keys := make(map[string]bool)
	uniq := []string{}
	for _, entry := range tags {
		if _, value := keys[entry]; !value {
			keys[entry] = true
			uniq = append(uniq, entry)
		}
	}

	return uniq
}
