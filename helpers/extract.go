package helpers

import (
	"bufio"
	"encoding/json"
	"fmt"
	"net/url"
	"os"
	"path"
	"strconv"
	"strings"

	"gitlab.com/kamee/picverse/logger"
)

func GetFromUrl(object string) (_data string) {
	_data = path.Base(object)
	return _data
}

func Get(object []byte, key string) (_data string, err error) {
	var ok bool
	m := make(map[string]interface{})
	err = json.Unmarshal(object, &m)
	if err != nil {
		return "", err
	}

	if key == "note" {
		_data, ok = m["object"].(map[string]interface{})["id"].(string)
		_data = path.Base(_data)
	} else if key == "object" {
		_data, ok = m["object"].(map[string]interface{})["object"].(string)
		_data = path.Base(_data)
	} else if key == "reply" {
		_data, ok = m["inReplyTo"].(string)
		_data = path.Base(_data)
	} else {
		_data, ok = m[key].(string)
		_data = path.Base(_data)
	}

	if !ok || _data == "" {
		err = fmt.Errorf("data not found")
		logger.Info.Println(err.Error())
		return "", err
	}

	return _data, err
}

func ObjectParser(object map[string]interface{}) (string, string, string) {
	remote_actor := object["actor"].(string)
	inbox := remote_actor + "/inbox"
	local_actor := object["object"].(string)

	return remote_actor, local_actor, inbox
}

func _convert(key interface{}) string {
	switch k := key.(type) {
	case int:
		return strconv.Itoa(k)
	case int64:
		return strconv.FormatInt(k, 10)
	}

	return key.(string)
}

func UniqueID(id, title string) string {
	_id := fmt.Sprintf("%s-%s", id, title)
	return _id
}

func MapPrinter(m []map[interface{}]interface{}) {
	for _, m := range m {
		for key, val := range m {
			fmt.Printf("%s: %s\n", _convert(key), val)
		}
	}
}

func BuildWebfinger(link string) string {
	url, err := url.Parse(link)
	if err != nil {
		logger.Warning.Println("Cannot create webfinger. Invalid URL: ", err.Error())
		return ""
	}
	hostname := strings.TrimPrefix(url.Hostname(), "www.")

	username := GetFromUrl(link)
	webfinger := fmt.Sprintf("%s@%s", username, hostname)

	return webfinger
}

func GetImagePath(link string) string {
	_url, err := url.Parse(link)
	if err != nil {
		return ""
	}

	path := strings.TrimPrefix(_url.Path, "/images/")
	return path
}

func LoadContent(filename string) (string, error) {
	var sb strings.Builder
	buf := []byte{}
	lineNumber := 1

	file, err := os.Open(filename)
	if err != nil {
		logger.Error.Printf("could not open the file: %v", err)
		return "", err
	}

	scanner := bufio.NewScanner(file)
	scanner.Buffer(buf, 2048*1024)

	for scanner.Scan() {
		sb.WriteString(scanner.Text())
		sb.WriteString("\n")
		lineNumber++
	}

	if err := scanner.Err(); err != nil {
		logger.Error.Fatalf("something bad happened in the line %v: %v", lineNumber, err)
		return "", err
	}

	return sb.String(), nil
}
