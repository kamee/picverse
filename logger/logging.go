package logger

import (
	"errors"
	"fmt"
	"github.com/robfig/cron"
	"io"
	"log"
	"os"
	"time"
	"path/filepath"
)

var (
	Info     *log.Logger
	Warning  *log.Logger
	Error    *log.Logger
	Critical *log.Logger
)

func create_log_file() *os.File {
	path := "logs"
	if _, err := os.Stat(path); errors.Is(err, os.ErrNotExist) {
		err := os.Mkdir(path, os.ModePerm)
		if err != nil {
			log.Println(err)
		}
	}

	dt := time.Now().Format("01-02-2006")
	filename := fmt.Sprintf("%s/picverse-%s.log", path, dt)
	file, err := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		log.Println("LOGGING: cannot create log file: ", err)
	}

	removeEmptyLogs(filename)

	return file
}

func StartLogger() {
	var file *os.File
	file = create_log_file()
	c := cron.New()
	log.Println("starting cron...")
	c.AddFunc("0 0 * * *", func() {
		file = create_log_file()
	})
	c.Start()

	multi := io.MultiWriter(file, os.Stdout)

	Info = log.New(multi,
		"INFO: ",
		log.Ldate|log.Ltime|log.Lshortfile)

	Warning = log.New(multi,
		"WARNING: ",
		log.Ldate|log.Ltime|log.Lshortfile)

	Error = log.New(multi,
		"ERROR: ",
		log.Ldate|log.Ltime|log.Lshortfile)

	Critical = log.New(multi,
		"ERROR: ",
		log.Ldate|log.Ltime|log.Lshortfile)
}

func removeEmptyLogs(current_filename string) {
	dir := "logs/"
	err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			log.Println(err)
		}
		
		if !info.IsDir() && info.Size() == 0 {
			if path != current_filename {
			err := os.Remove(path)
			if err != nil {
				log.Println(err)
			}
		}
		}
		return nil
	})
	if err != nil {
		log.Println(err)
	}
}
