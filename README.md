<div id="top"></div>
<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Don't forget to give the project a star!
*** Thanks again! Now go create something AMAZING! :D
-->
 
<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
<!-- [![Contributors][contributors-shield]][contributors-url] -->
<!-- [![Forks][forks-shield]][forks-url] -->
<!-- [![Stargazers][stars-shield]][stars-url] -->
<!-- [![Issues][issues-shield]][issues-url] -->
<!-- [![MIT License][license-shield]][license-url] -->



<!-- PROJECT LOGO -->
<br />
<div align="center">
  <h3 align="center">picverse</h3>
</div>
 
<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details> 

<!-- ABOUT THE PROJECT -->
### About The Project

ActivityPub based blogengine

<p align="right">(<a href="#top">back to top</a>)</p>
 
<!-- GETTING STARTED -->
## Getting Started

### Installation

0. if you want 
``
useradd -m picverse
passwd picverse
``
1. install go, nginx, certbot, postgres
2. git clone [project_link]
3. ```cp -r ../examples/config.json.example config/config.json``` 
then change dsn and host
4. ```mkdir /etc/nginx/sites-available```
& 
```mkdir /etc/nginx/sites-enabled```
5. ```sudo ufw allow 'Nginx HTTP```
6. create database and user in postgres
7. ```go build picverse/cmd/main.go```
8. ```./main picverse table```
9. ```git clone https://gitlab.com/kamee/picverse_template static```
10. ```mkdir keys```
11. ```openssl genrsa -out private.pem 2048```
12. ```openssl rsa -in private.pem -outform PEM -pubout -out public.pem```
13. ```vim /etc/nginx/sites-available/picverse.conf```
example:
```
upstream phoenix {
	server 127.0.0.1:8080 max_fails=5 fail_timeout=60s;
}

server {
	server_name   example.rip www.example.rip;

	listen         80;
	listen         [::]:80;

	location / {
		return  301 https://$server_name$request_uri;
	}
}

server {
	server_name example.rip www.example.rip;

   # [...certificate configs...]

	# the nginx default is 1m, not enough for large media uploads
	client_max_body_size 1m;
	ignore_invalid_headers off;

	proxy_http_version 1.1;
	proxy_set_header Upgrade $http_upgrade;
	proxy_set_header Connection "upgrade";
	proxy_set_header Host $http_host;
	proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

	location / {
		proxy_pass http://phoenix;
	}

	location ~ ^/(media|proxy) {
		slice              1m;
		proxy_cache_key    $host$uri$is_args$args$slice_range;
		proxy_set_header   Range $slice_range;
		proxy_cache_valid  200 206 301 304 1h;
		proxy_cache_lock   on;
		proxy_ignore_client_abort on;
		proxy_buffering    on;
		chunked_transfer_encoding on;
		proxy_pass         http://phoenix;
	}
}
```

14. ```./main -h ``` to see available command to create new actor, new post, etc.

<p align="right">(<a href="#top">back to top</a>)</p>
 
<!-- USAGE EXAMPLES -->
## Usage

create a new actor:

```./main -actor create```

create a new post:
```touch first_post.md```

```./main -note create```

create a post from images directory
images in directory ```images/[some_dir/example.png```

```./main -note img```

```./main -note create```

to see existing notes

```./main -note load```

### TODO

<p align="right">(<a href="#top">back to top</a>)</p>
 
<!-- ROADMAP -->
## Roadmap

### TODO

- [ ] Docs
- [ ] Spam handling
- [ ] Testing/Benchmarks
- [ ] Multi-language Support
- [ ] Create a logo
- [ ] Improve perfomance of image loading
- [ ] C2S
- [ ] add .env - read db config from there

### features

 - [x] Atom
 - [x] Sync with PubSub
 - [x] Sync with gemini capsule
 - [x] Convert posts to Gemini format
 - [x] Gallery mode


### bugs

- [ ] fetching issues for unicode paths
- [ ] some follow requests were not added to db

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- LICENSE -->
## License

Distributed under the GNU License. See `LICENSE.txt` for more information.

<p align="right">(<a href="#top">back to top</a>)</p>


<!-- CONTACT -->
## Contact

fedi - @kamee@vostain.net
mail - nokamee@protonmail.com

Project Links: 
[codeberg](https://codeberg.org/kamee/picverse)

<p align="right">(<a href="#top">back to top</a>)</p>
 
<!-- ACKNOWLEDGMENTS -->
## Acknowledgments
 
*  [go-fed/httpsig](https://github.com/go-fed/httpsig)

<p align="right">(<a href="#top">back to top</a>)</p>
