package config

import (
	"encoding/json"
	"os"

	"gitlab.com/kamee/picverse/logger"
)

type DatabaseConf struct {
	Host string
	DSN  string
}

type PodConf struct {
	Protocols []string
	Software  []map[string]string
	Message   string
	Motto     string
}

func DBConfig() *DatabaseConf {
	file, err := os.Open("config/config.json")
	if err != nil {
		logger.Critical.Fatal(err)
	}
	defer file.Close()
	decoder := json.NewDecoder(file)
	configuration := &DatabaseConf{}
	err = decoder.Decode(&configuration)
	if err != nil {
		logger.Critical.Fatal("Wrong configuration format: ", err.Error())
	}

	return configuration
}

func PodConfig() *PodConf {
	file, err := os.Open("pod.json")
	if err != nil {
		logger.Critical.Fatal(err)
	}
	defer file.Close()
	decoder := json.NewDecoder(file)
	configuration := &PodConf{}
	err = decoder.Decode(&configuration)
	if err != nil {
		logger.Critical.Fatal("Wrong configuration format: ", err.Error())
	}

	return configuration
}
