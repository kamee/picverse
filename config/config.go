package config

import (
	"fmt"
	"log"
	"github.com/joho/godotenv"
	"github.com/spf13/viper"

	"gitlab.com/kamee/picverse/logger"
)

type DatabaseConf struct {
	DSN    string
}

type PodConf struct {
	Host      string
	Author    string
	Email     string
	// protocols should be []string
	Protocols string
	Software  []map[string]string
	Message   string
	Motto     string
	Pubsub    bool
	NodeName  string
}

func DBConfig() *DatabaseConf {
	if err := godotenv.Load(); err != nil {
		logger.Critical.Println("Error loading .env file: %v", err)
	}

	viper.AutomaticEnv()
	viper.SetDefault("DB_HOST", "localhost")
	viper.SetDefault("DB_PORT", "5432")
	viper.SetDefault("DB_SSLMODE", "disable")

	user := viper.GetString("DB_USER")
	dbname := viper.GetString("DB_NAME")
	host := viper.GetString("DB_HOST")
	port := viper.GetString("DB_PORT")
	password := viper.GetString("DB_PASSWORD")
	sslmode := viper.GetString("DB_SSLMODE")

	if user == "" || dbname == "" || password == "" {
		logger.Critical.Println("DB_USER, DB_NAME, DB_PASSWORD values must be set on .env")
		return nil
	}

	dsn := fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=%s", user, password, host, port, dbname, sslmode)
	
	return &DatabaseConf{
		DSN: dsn,
	}
}

func PodConfig() *PodConf {
	if err := godotenv.Load(); err != nil {
		logger.Critical.Printf("Error loading .env file: %v", err)
	}

	viper.AutomaticEnv()
	viper.SetDefault("HOST", "localhost:8080")
	viper.SetDefault("AUTHOR", "anonym")
	viper.SetDefault("EMAIL", "email_is_not_set")
	viper.SetDefault("PROTOCOLS", "activitypub")
	viper.SetDefault("PUBSUB", "false")

	host := viper.GetString("HOST")
	author := viper.GetString("AUTHOR")
	protocols := viper.GetString("PROTOCOLS")
	email := viper.GetString("EMAIL")
	pubsub := viper.GetBool("PUBSUB")
	node_name := viper.GetString("NODE_NAME")

	if node_name == "" &&  pubsub {
		log.Fatalf("set NODE_NAME in .env or disable pubsub")
	}

	return &PodConf{
		Host: host,
		Email: email,
		Author: author,
		Protocols: protocols,
		Pubsub: pubsub,
		NodeName: node_name,
	}
}

