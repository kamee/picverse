package config

import (
	"fmt"
	"encoding/json"
	"os"
	"bufio"

	"gitlab.com/kamee/picverse/logger"
	gemini "codeberg.org/kamee/capsa"
)

type Gemini struct {
	Path     string `json:"path"`
	Enable   bool    `json:enable`
	NotePath string `json:"notepath"`
}

func InitGemini() {
	fmt.Print("input gemini server path: ")
	path_scan := bufio.NewScanner(os.Stdin)
	path_scan.Scan()
	err := path_scan.Err()
	if err != nil {
		logger.Error.Println(err)
	}

	path := path_scan.Text()

	fmt.Print("input file path where your posts are located: ")
	file_scan := bufio.NewScanner(os.Stdin)
	file_scan.Scan()
	err = file_scan.Err()
	if err != nil {
		logger.Error.Println(err)
	}

	notepath := file_scan.Text()
	gemini := Gemini{
		Path: path,
		NotePath: notepath,
		Enable: true,
	}
	jsonData, err := json.MarshalIndent(gemini, "", "  ")
	if err != nil {
		logger.Error.Println(err)
	}

	file, err := os.Create("config/gemini.json")
	if err != nil {
		logger.Error.Println(err)
	}
	defer file.Close()

	_, err = file.Write(jsonData)
	if err != nil {
		logger.Error.Println(err)
	}
}

func GeminiConfig() *Gemini {
	configuration := &Gemini{}
	file, err := os.Open("config/gemini.json")
	if err != nil {
		if os.IsNotExist(err) {
			configuration.Enable = false
			return configuration
		}
		logger.Critical.Fatal(err)
	}
	defer file.Close()
	decoder := json.NewDecoder(file)
	err = decoder.Decode(&configuration)
	if err != nil {
		logger.Critical.Fatal("Wrong configuration format: ", err.Error())
	}

	return configuration
}

func Convert() {
	file, err := os.Open("config/gemini.json")
	if err != nil {
		if os.IsNotExist(err) {
			logger.Critical.Fatal("config/gemini.json not exists. run -enable gemini to create")
		}
		logger.Critical.Fatal(err)
	}
	defer file.Close()
	decoder := json.NewDecoder(file)
	configuration := &Gemini{}
	err = decoder.Decode(&configuration)
	if err != nil {
		logger.Critical.Fatal("Wrong configuration format: ", err.Error())
	}

	// TODO: print failed files somewhere
	_, err = gemini.ConvertFiles(configuration.Path, configuration.NotePath)
	if err != nil {
		logger.Error.Println(err)
	}

	logger.Info.Println("converted!")
}


func ConvertSingleFile() {
	file, err := os.Open("config/gemini.json")
	if err != nil {
		if os.IsNotExist(err) {
			logger.Critical.Fatal("config/gemini.json not exists. run -enable gemini to create")
		}
		logger.Critical.Fatal(err)
	}
	defer file.Close()
	decoder := json.NewDecoder(file)
	configuration := &Gemini{}
	err = decoder.Decode(&configuration)
	if err != nil {
		logger.Critical.Fatal("Wrong configuration format: ", err.Error())
	}

	fmt.Print("input filepath you want to convert: ")
	path_scan := bufio.NewScanner(os.Stdin)
	path_scan.Scan()
	err = path_scan.Err()
	if err != nil {
		logger.Error.Println(err)
	}

	notepath := path_scan.Text()
	err = gemini.ConvertFile(configuration.Path, notepath)
	if err != nil {
		logger.Error.Println(err)
	}
}
