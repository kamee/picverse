package config

import (
	"context"
	"fmt"

	"gitlab.com/kamee/picverse/logger"
	pubsub "codeberg.org/kamee/pubsub-activitypub/wrapper"
	session "codeberg.org/kamee/pubsub-activitypub/session"
	"mellium.im/xmpp/mux"
)

func InitPubSub(nodename string) {
	fmt.Println(`Before starting please set XMPP_PASS, XMPP_ADDRESS, PUBSUB_NODE_HOST, NODE on .env. All your posts will be publish on your pubsub node as well.`)

	if nodename != "" {
		session, err := session.GetSession()
		if err != nil {}
		logger.Info.Printf("creating a new node %s", nodename)
   	        mux := mux.New("jabber:client")
	        go func() {
			err := session.Serve(mux)
			if err != nil {
				logger.Info.Printf("error handling session responses: %v", err)
			}
		}()
		pubsub.CreateNodeWrapper(context.Background(), nodename, session)
		defer func() {
			if err := session.Close(); err != nil {
				logger.Info.Printf("error ending session: %v", err)
			}
			if err := session.Conn().Close(); err != nil {
				logger.Info.Printf("error closing connection: %v", err)
			}
		}()
	}
	logger.Info.Println("to send messages add PUBSUB=true on your .env")
}

